"""Example script for flow past a sphere"""

import nice.mesh.api as mesh
from nice.solver.solver import EDACSolver2D
from nice.solver.boundary_conditions import CGVelocityDirichlet, HomogenousNeumannPressure2D, \
    CGVelocityHomogeneousNeumann, DirichletPressure2D

from nice.ibm.api import Geometry2D, ImmersedBoundaryManager

# domain and physical constants
dim=2
xmin, ymin, zmin = -5.0, -2.5, 0
xmax, ymax, zmax = 25.0, 2.5, 1
dx = dy = dz = 0.02
Re = 250.0
uinflow = 1.0

# We will use a standard Staggered Grid discretization
dtype = EDACSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype, is_staggered=False )

# construct the solver
solver = EDACSolver2D(
    dim=dim, grid=grid, Re=Re, pfreq=1000, steady_state=False, tf=10.0, cfl=0.3)

solver.set_mach_number(0.1)

# create the immersed boundary manager for the solver
Geometry2D.write_cylinder(rad=1.0, npanels=120)
geom = Geometry2D('cylinder.dat')
ibm_manager = ImmersedBoundaryManager(grid, geom)

solver.set_immersed_boundary_manager(ibm_manager, is_moving=False)

# set the boundary conditions 
solver.set_bc_north( CGVelocityHomogeneousNeumann(grid) )
solver.set_bc_south( CGVelocityHomogeneousNeumann(grid) )
solver.set_bc_east(  CGVelocityHomogeneousNeumann(grid) )

# inlet boundary conditions (Dirichlet)
solver.set_bc_west(  CGVelocityDirichlet(grid, u=1.0, v=0.0) )

# set boundary conditions for pressure
solver.set_pressure_bc_north( HomogenousNeumannPressure2D(grid) )
solver.set_pressure_bc_south( HomogenousNeumannPressure2D(grid) )
solver.set_pressure_bc_west(  HomogenousNeumannPressure2D(grid) )
solver.set_pressure_bc_east(  DirichletPressure2D(grid) )

# initialize the problem
u = grid.get_field('u0')[0]; u[:] = uinflow
v = grid.get_field('v0')[0]; v[:] = 0.0
p = grid.get_field('p0')[0]; p[:] = 0.0

# solve
solver.setup()
solver.solve()
