"""Double shear layer problem with the EDAC scheme"""

import numpy
import nice.mesh.api as mesh
from nice.solver.solver import EDACSolver2D
from nice.solver.boundary_conditions import CGVelocityPeriodic, PeriodicPressure2D

# domain and physical constants
dim = 2
xmin, ymin, zmin = 0,  0,  0
xmax, ymax, zmax = 1.0,  1.0, 1.0

dx = dy = dz = 1.0/128

# EDAC type discretization on a collocated grid
dtype = EDACSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype )

# construct the solver
solver = EDACSolver2D(
    dim=dim, grid=grid, Re=100.0, pfreq=1000, cfl=0.3, tf=5.0)

solver.set_mach_number(0.2)

# set the boundary conditions 
solver.set_bc_north( CGVelocityPeriodic(grid) )
solver.set_bc_south( CGVelocityPeriodic(grid) )
solver.set_bc_east(  CGVelocityPeriodic(grid) )
solver.set_bc_west(  CGVelocityPeriodic(grid) )

# set boundary conditions for pressure
solver.set_pressure_bc_north( PeriodicPressure2D(grid) )
solver.set_pressure_bc_south( PeriodicPressure2D(grid) )
solver.set_pressure_bc_east(  PeriodicPressure2D(grid) )
solver.set_pressure_bc_west(  PeriodicPressure2D(grid) )

# initialize the problem
cents = grid.get_field('centroid')[0]
xc = cents[:, :, 0]; yc = cents[:, :, 1]
u = grid.get_field('u0')[0]
v = grid.get_field('v0')[0]
p = grid.get_field('p0')[0]

pi = numpy.pi; cos = numpy.cos; sin = numpy.sin
for j in range( 1, grid.jmax ):
    for i in range( 1, grid.jmax ):
        x = xc[j, i]; y = yc[j, i]
        
        u[j, i] = -1.0 * cos(2*pi*x) * sin(2*pi*y)
        v[j, i] = +1.0 * sin(2*pi*x) * cos(2*pi*y)

# solve
solver.setup()
solver.solve()
