"""Example script for the Lid Driven Cavity problem"""

import nice.mesh.api as mesh
from nice.solver.solver import SGPressureCorrectionSolver2D
from nice.solver.boundary_conditions import SGVelocityDirichlet2D
from nice.ibm.api import Geometry2D, ImmersedBoundaryManager

# domain and physical constants
dim = 2
dx = dy = dz = 1./64
xmin, ymin, zmin = 0,  0,  0
xmax, ymax, zmax = 1,  1,  1

# We will use a standard Staggered Grid discretization
dtype = SGPressureCorrectionSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype, is_staggered=True )

# construct the solver
solver = SGPressureCorrectionSolver2D(
    dim=dim, grid=grid, Re=1000.0, pfreq=1000, steady_state=True)

solver.set_steady_state_tolerance(1e-8)
solver.set_donor_cell_discretization_gamma(gamma=0.5)

# create the immersed boundary for the solver
Geometry2D.write_box(xc=0.5, yc=0.5, a=0.25, b=0.25)
geom = Geometry2D('box.dat')
ibm_manager = ImmersedBoundaryManager(grid, geom)
solver.set_immersed_boundary_manager(ibm_manager, is_moving=False)

# set the boundary conditions 
solver.set_bc_north( SGVelocityDirichlet2D(grid, u=1.0, v=0.0) )
solver.set_bc_south( SGVelocityDirichlet2D(grid, u=0.0, v=0.0) )
solver.set_bc_west(  SGVelocityDirichlet2D(grid, u=0.0, v=0.0) )
solver.set_bc_east(  SGVelocityDirichlet2D(grid, u=0.0, v=0.0) )

# initialize the problem
u = grid.get_field('u0')[0]; u[:] = 0.0
v = grid.get_field('v0')[0]; v[:] = 0.0
p = grid.get_field('p')[0]; p[:] = 0.0

# solve
solver.setup()
solver.solve()
