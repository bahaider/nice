"""Example script for the Lid Driven Cavity problem on a Collocated grid with Pressure Correction"""

import nice.mesh.api as mesh
from nice.solver.solver import CGPressureCorrectionSolver2D
from nice.solver.boundary_conditions import CGVelocityDirichlet

# domain and physical constants
dim = 2
dx = dy = dz = 1./64
xmin, ymin, zmin = 0,  0,  0
xmax, ymax, zmax = 1,  1,  1

# We will use a standard Staggered Grid discretization
dtype = CGPressureCorrectionSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype )

# construct the solver
solver = CGPressureCorrectionSolver2D(
    dim=dim, grid=grid, Re=5000.0, pfreq=1000, steady_state=True)

solver.set_steady_state_tolerance(1e-8)

# set the boundary conditions 
solver.set_bc_north( CGVelocityDirichlet(grid, u=1.0, v=0.0) )
solver.set_bc_south( CGVelocityDirichlet(grid, u=0.0, v=0.0) )
solver.set_bc_west(  CGVelocityDirichlet(grid, u=0.0, v=0.0) )
solver.set_bc_east(  CGVelocityDirichlet(grid, u=0.0, v=0.0) )

# initialize the problem
u = grid.get_field('u0')[0]; u[:] = 0.0
v = grid.get_field('v0')[0]; v[:] = 0.0
p = grid.get_field('p')[0];  p[:] = 0.0

# solve
solver.setup()
solver.solve()
