#!/usr/bin/env python
from setuptools import find_packages, setup
from Cython.Distutils import build_ext
from Cython.Build import cythonize
from numpy.distutils.extension import Extension
import sys
import os
import numpy

# Add additional include directories here
inc_dirs = [numpy.get_include()]

# Add additional compile and link arguments here
extra_compile_args = []
extra_link_args = []

cy_directives = {'embedsignature':True,}

# common includes
common_include_dir = [ os.path.abspath('./src/nice/mesh/') ]
inc_dirs += common_include_dir

# Extension modules for NICE                 
ext_modules = [
    Extension(name="nice.mesh.grid",
              sources=["src/nice/mesh/grid.pyx"],
              include_dirs=inc_dirs),

    Extension(name="nice.solver.poisson",
              sources=["src/nice/solver/poisson.pyx"]),

    Extension(name="nice.solver.integrator",
              sources=["src/nice/solver/integrator.pyx"]),

    Extension(name="nice.solver.schemes",
              sources=["src/nice/solver/schemes.pyx"]),

    Extension(name="nice.solver.boundary_conditions",
              sources=["src/nice/solver/boundary_conditions.pyx"]),

    Extension(name="nice.ibm.ibm_manager",
              sources=["src/nice/ibm/ibm_manager.pyx"]),

    ]

# default treatment for all extensions
for extn in ext_modules:
    extn.include_dirs = inc_dirs
    extn.extra_compile_args = extra_compile_args
    extn.extra_link_args = extra_link_args
    extn.pyrex_directives = cy_directives
    extn.language = 'c'

setup(name="nice",
      version="0.0",
      package_data={'': ['*.pxd']},
      include_package_data=True,
      zip_safe=False,
      ext_modules = ext_modules,
      cmdclass={'build_ext': build_ext},
      package_dir = {'': 'src'},
      )
