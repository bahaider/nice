"""Declarations for the different integrators used in the FD scheme"""
from nice.mesh.grid cimport NpyGrid

import numpy as np
cimport numpy as np

cdef class CollocatedGrid2D:
    cdef public NpyGrid grid
    cdef public int imax, jmax

    cdef public double Re, Rei
    cdef public double Ma, Mai
    cdef public double gamma

    cdef public double delp
    cdef public double ke, delke
    cdef public int niter
    cdef public double max_div

    ################# Member functions #################
    cdef _divergence(
        self, np.ndarray[ndim=2, dtype=np.float64_t] u,
        np.ndarray[ndim=2, dtype=np.float64_t] v,
        np.ndarray[ndim=2, dtype=np.float64_t] res)

    cdef _laplacian(
        self, np.ndarray[ndim=2, dtype=np.float64_t] phi, 
        np.ndarray[ndim=2, dtype=np.float64_t] res)

    cdef _gradient(
        self, 
        np.ndarray[ndim=2, dtype=np.float64_t] phi,
        np.ndarray[ndim=2, dtype=np.float64_t] phix,
        np.ndarray[ndim=2, dtype=np.float64_t] phiy,
        )

    cpdef compute_stats(self)

cdef class EDAC2D(CollocatedGrid2D):
    cpdef compute_rhs(self, double dt, int stage=*, bint penalty_ibm=*)
    cpdef update(self, double dt, int stage, int nstages=*)

cdef class CGPressureCorrection2D:
    cdef public NpyGrid grid              # underlying grid object
    cdef public int imax, jmax            # Grid indices

    cdef public double Re, Rei            # Reynolds number and it's inverse
    cdef public double gamma              # donor cell discretization constant

    cdef public double delp               # change in pressure
    cdef public double ke, delke          # kinetic energy and change

    ###################### Member Functions #########################

    # interpolate face velocities
    cpdef interpolate_face_velocities(self, double dt)

    # Get the intermediate velocity
    cpdef compute_vstar(self, double dt, double a1, double a2)

    # Set boundary conditions for the intermediate velocity
    cpdef vstar_boundary_conditions(self)
    
    # compute the RHS of the PPE
    cpdef compute_rhs_ppe(self, double dt)

    # Solver the PPE using the GSSOR method
    cpdef int solve_ppe_gssor(self)

    # Velocity correction
    cpdef correct_velocity(self, double dt)

    # Compute change in pressure and kinetic energy
    cpdef compute_stats(self)

cdef class SGPCVanKannIntegrator2D:
    cdef public NpyGrid grid              # underlying grid
    cdef public object coeffmat_ppe       # ppe coefficient matrix
    cdef public np.ndarray rhs_ppe        # ppe rhs vector
    cdef public bint ibm_force            # flag for ibm force
    cdef public double uinflow            # inflow velocity
    
    cdef public bint moving_boundary      # flag for moving boundaries

    cdef public int boundary_condition    # boundary conditions

    # method to get the predicted velocities
    cpdef get_vstar(self, double Re, double dt, int iteration)

    # method to add ibm force
    cpdef add_ibm_force(self, double dt)

    # method to set the boundary condition
    cpdef set_boundary_vstar(self)
    
    # set-up the right hand side for the ppe
    cpdef set_rhs_ppe(self, double dt)

    # set-up the coefficient matrix for the ppe
    cpdef _coeffmat_inflow(self)
    cpdef _coeffmat_ldc(self)

    # solve the ppe to get the pressure correction vector
    cpdef solve_ppe(self)

    # update pressure and velocity using pcorr
    cpdef update_pv(self, double dt)    
