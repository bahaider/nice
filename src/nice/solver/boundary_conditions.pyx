#!python
#cython: boundscheck=False, wraparound=False, cdivision=True

"""Implementation for the Boundary conditions"""

import numpy as np
cimport numpy as np

from libc.math cimport tanh

cdef class BoundaryCondition:
    def __init__(self, NpyGrid grid):
        self.grid = grid
        self.face = ""

    def set_face(self, face):
        self.face = face
        
    cpdef apply_bc(self, int stage=0):
        pass

cdef class SGVelocityDirichlet2D(BoundaryCondition):
    def __init__(self, NpyGrid grid, double u=0.0, double v=0.0):
        self.u = u
        self.v = v
        super(SGVelocityDirichlet2D, self).__init__(grid)

    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] uc = grid.grid['uc'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vc = grid.grid['vc'][0]
        
        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        # locals
        cdef int i, j
        
        # left face. U velocity variables are specified along this
        # wall and v velocity variables will be interpolated.
        if self.face == 'W':
            for j in range(1, jmax + 1):
                u[j, 0] = self.u
                v[j, 0] = 2*self.v - v[j, 1]

                uc[j, 0] = 2*self.u - uc[j, 1]
                vc[j, 0] = 2*self.v - vc[j, 1]
                
        # right face: U velocity is specified and V will be interpolated
        if self.face == 'E':
            for j in range(1, jmax + 1):
                u[j, imax] = self.u
                v[j, imax+1] = 2*self.v - v[j, imax]

                uc[j, imax+1] = 2*self.u - uc[j, imax]
                vc[j, imax+1] = 2*self.v - vc[j, imax]
                
        # south face: V velocity is specified and U will be interpolated
        if self.face == 'S':
            for i in range(1, imax + 1):
                v[0, i] = self.v
                u[0, i] = 2*self.u - u[1, i]

                uc[0, i] = 2*self.u - uc[1, i]
                vc[0, i] = 2*self.v - vc[1, i]

        # north face: V velocity is specified and U will be interpolated
        if self.face == 'N':
            for i in range(1, imax+1):
                v[jmax, i] = self.v
                u[jmax+1, i] = 2*self.u - u[jmax, i]

                uc[jmax+1, i] = 2*self.u - uc[jmax, i]
                vc[jmax+1, i] = 2*self.v - vc[jmax, i]

cdef class SGVelocityHomogenousNeumann2D(BoundaryCondition):
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] uc = grid.grid['uc'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vc = grid.grid['vc'][0]
        
        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        # locals
        cdef int i, j
        
        # left face. U (normal to wall) velocity variables are set to
        # 0 and V velocity (tangential to wall) variabels are set to
        # have 0 derivative
        if self.face == 'W':
            for j in range(1, jmax + 1):
                u[j, 0] = 0.0
                v[j, 0] = v[j, 1]

                uc[j, 0] = uc[j, 1]
                vc[j, 0] = vc[j, 1]
                
        # right face. U (normal to wall) velocity variables are set to
        # 0 and V velocity (tangential to wall) variabels are set to
        # have 0 derivative
        if self.face == 'E':
            for j in range(1, jmax + 1):
                u[j, imax] = 0.0
                v[j, imax+1] = v[j, imax]

                uc[j, imax+1] = uc[j, imax]
                vc[j, imax+1] = vc[j, imax]
                
        # south face: V velocity (normal component) variable is set to
        # 0 and U (tangential component) is set so that there is 0
        # derivative
        if self.face == 'S':
            for i in range(1, imax + 1):
                v[0, i] = 0.0
                u[0, i] = u[1, i]

                uc[0, i] = uc[1, i]
                vc[0, i] = vc[1, i]

        # north face: V velocity (normal component) variable is set to
        # 0 and U (tangential component) is set so that there is 0
        # derivative
        if self.face == 'N':
            for i in range(1, imax+1):
                v[jmax, i] = 0.0
                u[jmax+1, i] = u[jmax, i]

                uc[jmax+1, i] = uc[jmax, i]
                vc[jmax+1, i] = vc[jmax, i]

cdef class SGVelocityOutflow2D(BoundaryCondition):
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] uc = grid.grid['uc'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vc = grid.grid['vc'][0]
        
        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        # locals
        cdef int i, j
        
        # left face.
        if self.face == 'W':
            for j in range(1, jmax + 1):
                u[j, 0] = u[j, 1]
                v[j, 0] = v[j, 1]
                
        # right face.
        if self.face == 'E':
            for j in range(1, jmax + 1):
                u[j, imax] = u[j, imax-1]
                v[j, imax+1] = v[j, imax]
                
        # south face:
        if self.face == 'S':
            for i in range(1, imax + 1):
                v[0, i] = v[1, i]
                u[0, i] = u[1, i]

        # north face:
        if self.face == 'N':
            for i in range(1, imax+1):
                v[jmax, i] = v[jmax-1, i]
                u[jmax+1, i] = u[jmax, i]

cdef class CGVelocityPeriodic(BoundaryCondition):
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]

        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        # locals
        cdef int i, j

        if self.face == "W":
            for j in range(1, jmax+1):
                u[j, 0] = u[j, imax]
                v[j, 0] = v[j, imax]

        if self.face == "E":
            for j in range(1, jmax+1):
                u[j, imax+1] = u[j, 1]
                v[j, imax+1] = v[j, 1]
            
        if self.face == "S":
            for i in range(1, imax+1):
                u[0, i] = u[jmax, i]
                v[0, i] = v[jmax, i]

        if self.face == "N":
            for i in range(1, imax+1):
                u[jmax+1, i] = u[1, i]
                v[jmax+1, i] = v[1, i]

cdef class CGVelocityHomogeneousNeumann(BoundaryCondition):
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]

        # locals
        cdef int i, j

        if self.face == "W":
            for j in range(1, jmax+1):
                u[j, 0] = u[j, 1]
                v[j, 0] = v[j, 1]

        if self.face == "E":
            for j in range(1, jmax+1):
                u[j, imax+1] = u[j, imax]
                v[j, imax+1] = v[j, imax]

        if self.face == "N":
            for i in range(1, imax+1):
                u[jmax+1, i] = u[jmax, i]
                v[jmax+1, i] = v[jmax, i]

        if self.face == "S":
            for i in range(1, imax+1):
                u[0, i] = u[1, i]
                v[0, i] = v[1, i]
                
cdef class PeriodicPressure2D(BoundaryCondition):
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p%d'%stage][0]

        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        # locals
        cdef int i, j

        if self.face == "W":
            for j in range(1, jmax+1):
                p[j, 0] = p[j, imax]

        if self.face == "E":
            for j in range(1, jmax+1):
                p[j, imax+1] = p[j, 1]
            
        if self.face == "S":
            for i in range(1, imax+1):
                p[0, i] = p[jmax, i]

        if self.face == "N":
            for i in range(1, imax+1):
                p[jmax+1, i] = p[1, i]

cdef class CGVelocityDirichlet(BoundaryCondition):
    def __init__(self, NpyGrid grid, double u=0.0, double v=0.0):
        self.u = u
        self.v = v

        super(CGVelocityDirichlet, self).__init__(grid)

    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]
        
        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        # locals
        cdef int i, j
        
        # left face. U velocity variables are specified along this
        if self.face == 'W':
            for j in range(1, jmax + 1):
                u[j, 0] = 2*self.u - u[j, 1]
                v[j, 0] = 2*self.v - v[j, 1]
                
        # right face: U velocity is specified and V will be interpolated
        if self.face == 'E':
            for j in range(1, jmax + 1):
                u[j, imax+1] = 2*self.u - u[j, imax]
                v[j, imax+1] = 2*self.v - v[j, imax]
                
        # south face: V velocity is specified and U will be interpolated
        if self.face == 'S':
            for i in range(1, imax + 1):
                v[0, i] = 2*self.v - v[1, i]
                u[0, i] = 2*self.u - u[1, i]

        # north face: V velocity is specified and U will be interpolated
        if self.face == 'N':
            for i in range(1, imax+1):
                v[jmax+1, i] = 2*self.v - v[jmax, i]
                u[jmax+1, i] = 2*self.u - u[jmax, i]

cdef class HomogenousNeumannPressure2D(BoundaryCondition):
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p%d'%stage][0]
        
        # locals
        cdef int i, j

        if self.face == 'W':
            for j in range(1, jmax+1):
                p[j, 0] = p[j, 1]

        if self.face == 'E':
            for j in range(1, jmax+1):
                p[j, imax+1] = p[j, imax]

        if self.face == 'S':
            for i in range(1, imax+1):
                p[0, i] = p[1, i]

        if self.face == 'N':
            for i in range(1, imax+1):
                p[jmax+1, i] = p[jmax, i]

cdef class DirichletPressure2D(BoundaryCondition):
    def __init__(self, NpyGrid grid, pressure=0.0):
        self.pressure = pressure
        
        super(DirichletPressure2D, self).__init__(grid)
        
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p%d'%stage][0]
        
        # locals
        cdef int i, j

        if self.face == 'W':
            for j in range(1, jmax+1):
                p[j, 0] = self.pressure

        if self.face == 'E':
            for j in range(1, jmax+1):
                p[j, imax+1] = self.pressure

        if self.face == 'S':
            for i in range(1, imax+1):
                p[0, i] = self.pressure

        if self.face == 'N':
            for i in range(1, imax+1):
                p[jmax+1, i] = self.pressure

cdef class DKCDirichletPressure2D(BoundaryCondition):
    def __init__(self, NpyGrid grid, double Re=1.0, double delta=0.1, double u0=1.0):
        self.Re = Re
        self.delta = delta
        self.u0 = u0
        
        super(DKCDirichletPressure2D, self).__init__(grid)
        
    cpdef apply_bc(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef int imax = grid.imax
        cdef int jmax = grid.jmax

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]
        
        # locals
        cdef int i, j

        cdef double Rei = 1./self.Re

        cdef double udotn
        cdef double vmag2
        cdef double s0
        cdef double udeltai = 1./(self.delta*self.u0)

        if self.face == 'W':
            for j in range(1, jmax+1):
                p[j, 0] = self.pressure

        if self.face == 'E':
            for j in range(1, jmax+1):

                udotn = u[j, 0]
                vmag2 = u[j, 0]*u[j, 0] + v[j, 0]*v[j, 0]
                s0 = 0.5 * (1 - tanh(udeltai*udotn) )

                p[j, imax+1] = -0.5*vmag2*s0

        if self.face == 'S':
            for i in range(1, imax+1):
                p[0, i] = self.pressure

        if self.face == 'N':
            for i in range(1, imax+1):
                p[jmax+1, i] = self.pressure
