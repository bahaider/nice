import numpy as np
cimport numpy as np

cimport cython
from libc.math cimport fabs, sqrt

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef int gssor2D(
    np.ndarray[ndim=2, dtype=np.float64_t] phi,
    np.ndarray[ndim=2, dtype=np.float64_t] phi_new,
    np.ndarray[ndim=2, dtype=np.float64_t] rhs,
    double dx, double dy, int jmax, int imax, int ncells_physical,
    double tol=1e-8, int max_iter=100000,
    double omega=1.7, bint dirichlet_outlet=False):
    r"""Solution using the SRJ algorithm

    Parameters:
    
    phi, phi_new : numpy.ndarray
        Solution and buffer array

    rhs : numpy.ndarray
        Poisson source term

    dx, dy : float
        Spatial discretizations along X (cols) & Y (rows)
    
    jmax, imax : int
        Number of interior nodes or grid points along Y/X

    ncells_physical : int
        Number of cells to compute the error

    tol : float
        Convergence tolerance

    max_iter : int
        Maximum number of iterations
    
    omega : double
        SOR relaxation parameter

    """

    # locals
    cdef int i, j, niter_inner, niter_total = 0
    cdef double error = 1.0

    cdef double dxi = 1./dx
    cdef double dxi2 = dxi*dxi

    cdef double dyi = 1./dy
    cdef double dyi2 = dyi*dyi

    cdef double denominator = 1.0/(2*dxi2 + 2*dyi2)

    while( (error > tol) and (niter_total < max_iter) ):
        niter_total += 1
        error = 0.0
        
        # Now iterate over the original nodes
        for i in range(1, imax+1):
            for j in range(1, jmax+1):
                
                phi_new[j, i] = (1.0 - omega)*phi[j, i] + \
                                ( omega*dxi2*(phi_new[j, i-1] + phi[j, i+1]) + \
                                  omega*dyi2*(phi_new[j-1, i] + phi[j+1, i]) - \
                                  omega*rhs[j, i] ) * denominator

                # incrementally compute the norm of the error
                #error = error + ( phi[j, i] - phi_new[j, i] ) * ( phi[j, i] - phi_new[j, i] )
                error = error + fabs( phi[j, i] - phi_new[j, i] )
                
        # boundary conditions for the pressure equation
        for i in range(1, imax+1):
            phi_new[0, i] = phi[1, i]
            phi_new[jmax+1, i] = phi[jmax, i]

        for j in range(1, jmax+1):
            phi_new[j, 0] = phi[j, 1]

            if not dirichlet_outlet:
                phi_new[j, imax+1] = phi[j, imax]

        # check for convergence
        #if sqrt(error*ncellsi) < tol:
        if error/ncells_physical < tol:
            break

        # swap buffers for the next iteration
        phi[:] = phi_new[:]

    # return the total number of iterations
    return niter_total
