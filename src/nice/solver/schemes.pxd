"""Definitions for the Incompressible NS schemes"""

from nice.mesh.grid cimport NpyGrid

import numpy as np
cimport numpy as np

cdef class SGPressureCorrection2D:
    cdef public NpyGrid grid              # underlying grid object
    cdef public int imax, jmax            # Grid indices

    cdef public double Re, Rei            # Reynolds number and it's inverse
    cdef public double gamma              # donor cell discretization constant

    cdef public double max_div            # maximum divergence 
    cdef public double delp               # change in pressure
    cdef public double ke, delke          # kinetic energy and change
    cdef public int niter                 # number of iterations for the PPE

    ###################### Member Functions #########################

    # Get the intermediate velocity
    cpdef compute_vstar(self, double dt, double a1, double a2, int stage=*, bint penalty_ibm=*)

    # Set boundary conditions for the intermediate velocity
    cpdef vstar_boundary_conditions(self, int stage=*)
    
    # compute the RHS of the PPE
    cpdef compute_rhs_ppe(self, double dt)

    # Solver the PPE using the GSSOR method
    cpdef solve_ppe_gssor(self, int max_iter=*, bint outlet_pressure_dirichlet=*)

    # Velocity correction
    cpdef correct_velocity(self, double dt, int stage=*)

    # Compute change in pressure and kinetic energy
    cpdef compute_stats(self, int stage=*)

    # Interpolate velocities to the cell center
    cpdef interpolate_velocity(self, int stage=*)
