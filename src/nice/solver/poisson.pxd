import numpy as np
cimport numpy as np

cdef int gssor2D(
    np.ndarray[ndim=2, dtype=np.float64_t] phi,
    np.ndarray[ndim=2, dtype=np.float64_t] phi_new,
    np.ndarray[ndim=2, dtype=np.float64_t] rhs,
    double dx, double dy, int jmax, int imax, int ncells_physical,
    double tol=*, int max_iter=*,
    double omega=*, bint dirichlet_outlet=*)
