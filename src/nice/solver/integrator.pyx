"""Definitions for the integrators used in NICE

The Integrator is responsible for advancing the solution to the next
time step. It interfaces with the scheme to advance the incompressible
Navier Stokes equations (pressure correction, PISO etc) and the method
used to include the forces due to the immersed boundary.

"""
import numpy as np
cimport numpy as np

from scipy import sparse
import scipy.sparse.linalg as linalg

from libc.math cimport fabs, sqrt, fmax
cimport cython

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef int gssor2D(
    np.ndarray[ndim=2, dtype=np.float64_t] phi,
    np.ndarray[ndim=2, dtype=np.float64_t] phi_new,
    np.ndarray[ndim=2, dtype=np.float64_t] rhs,
    double dx, double dy, int jmax, int imax, int ncells_physical,
    double tol=1e-8, int max_iter=100000,
    double omega=1.7):
    r"""Solution using the SRJ algorithm

    Parameters:
    
    phi, phi_new : numpy.ndarray
        Solution and buffer array

    rhs : numpy.ndarray
        Poisson source term

    dx, dy : float
        Spatial discretizations along X (cols) & Y (rows)
    
    jmax, imax : int
        Number of interior nodes or grid points along Y/X

    ncells_physical : int
        Number of cells to compute the error

    tol : float
        Convergence tolerance

    max_iter : int
        Maximum number of iterations
    
    omega : double
        SOR relaxation parameter

    """

    # locals
    cdef int i, j, niter_inner, niter_total = 0
    cdef double error = 1.0

    cdef double dxi = 1./dx
    cdef double dxi2 = dxi*dxi

    cdef double dyi = 1./dy
    cdef double dyi2 = dyi*dyi

    cdef double denominator = 1.0/(2*dxi2 + 2*dyi2)

    while( (error > tol) and (niter_total < max_iter) ):
        niter_total += 1
        error = 0.0
        
        # Now iterate over the original nodes
        for i in range(1, imax+1):
            for j in range(1, jmax+1):
                
                phi_new[j, i] = (1.0 - omega)*phi[j, i] + \
                                ( omega*dxi2*(phi_new[j, i-1] + phi[j, i+1]) + \
                                  omega*dyi2*(phi_new[j-1, i] + phi[j+1, i]) - \
                                  omega*rhs[j, i] ) * denominator

                # incrementally compute the norm of the error
                #error = error + ( phi[j, i] - phi_new[j, i] ) * ( phi[j, i] - phi_new[j, i] )
                error = error + fabs( phi[j, i] - phi_new[j, i] )
                
        # boundary conditions for the pressure equation
        for i in range(1, imax+1):
            phi_new[0, i] = phi[1, i]
            phi_new[jmax+1, i] = phi[jmax, i]

        for j in range(1, jmax+1):
            phi_new[j, 0] = phi[j, 1]
            phi_new[j, imax+1] = phi[j, imax]

        # check for convergence
        #if sqrt(error*ncellsi) < tol:
        if error/ncells_physical < tol:
            break

        # swap buffers for the next iteration
        phi[:] = phi_new[:]

    # return the total number of iterations
    return niter_total

cdef class CollocatedGrid2D:
    """Base class for schemes using a collocated grid"""
    def __init__(self, NpyGrid grid, double Re=1000.0):
        self.grid = grid
        self.Re = Re
        self.Rei = 1./Re

        # index ranges
        self.imax = grid.imax
        self.jmax = grid.jmax
        
        self.ke = 1.0
        self.delke = 0.0
        self.delp = 0.0
        self.niter = 0

        self.max_div = 0.0

    cdef _divergence(
        self, np.ndarray[ndim=2, dtype=np.float64_t] u,
        np.ndarray[ndim=2, dtype=np.float64_t] v,
        np.ndarray[ndim=2, dtype=np.float64_t] res):

        cdef int imax = self.imax
        cdef int jmax = self.jmax
        
        cdef double dxi = 1./self.grid.dx
        cdef double dyi = 1./self.grid.dy

        cdef int i, j

        for i in range(1, imax+1):
            for j in range(1, jmax+1):
                res[j, i] = 0.5*dxi*(u[j, i+1] - u[j, i-1]) + 0.5*dyi*(v[j+1, i] - v[j-1, i])

    cdef _laplacian(
        self, np.ndarray[ndim=2, dtype=np.float64_t] phi, 
        np.ndarray[ndim=2, dtype=np.float64_t] res):

        cdef int i, j

        cdef double dxi = 1./self.grid.dx
        cdef double dyi = 1./self.grid.dy

        cdef double dxi2 = dxi*dxi
        cdef double dyi2 = dyi*dyi

        for i in range(1, self.imax + 1):
            for j in range(1, self.jmax + 1):

                res[j, i] = dxi2*(phi[j, i+1] - 2*phi[j, i] + phi[j, i-1]) + \
                            dyi2*(phi[j+1, i] - 2*phi[j, i] + phi[j-1, i])

    cdef _gradient(
        self, 
        np.ndarray[ndim=2, dtype=np.float64_t] phi,
        np.ndarray[ndim=2, dtype=np.float64_t] phix,
        np.ndarray[ndim=2, dtype=np.float64_t] phiy,
        ):
                
        cdef NpyGrid grid = self.grid
        
        cdef int i, j
        
        cdef double dxi = 1./self.grid.dx
        cdef double dyi = 1./self.grid.dy

        for i in range(1, self.imax+1):
            for j in range(1, self.jmax+1):
                phix[j, i] = 0.5*dxi * (phi[j, i+1] - phi[j, i-1])
                phiy[j, i] = 0.5*dyi * (phi[j+1, i] - phi[j-1, i])

    cpdef compute_stats(self):
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v0'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] vort = grid.grid['vort'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] div = grid.grid['div'][0]

        cdef int i, j
        cdef double _ke = 0.0
        cdef double u2, v2, vmag2

        cdef double dx = grid.dx
        cdef double dy = grid.dy

        cdef double dxi = 0.5/grid.dx
        cdef double dyi = 0.5/grid.dy
        cdef double max_div = -1e100

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):
                # sum kinetic energy
                u2 = u[j,i] * u[j,i]
                v2 = v[j,i] * v[j,i]
                
                vmag2 = u2 + v2
                _ke = _ke + 0.5 * vmag2 * dx*dy

                vort[j, i] = dyi * (u[j+1, i] - u[j-1, i]) - dxi * (v[j, i+1] - v[j, i-1])
                div[j, j] = dxi*(u[j,i+1]-u[j,i-1]) + dyi*(v[j+1,i]-v[j-1,i])

                max_div = fmax(max_div, div[j,i])

        _ke /= grid.volume

        self.delke = fabs(_ke - self.ke)
        self.ke = _ke
        grid.ke = _ke
        self.max_div = max_div

cdef class CGPressureCorrection2D:
    """Base class for schemes using the Pressure correction method to
    solve the incompressible Navier Stokes equations on a 2D
    collocated grid.

    """
    def __init__(self, NpyGrid grid, double Re=1000.0):
        self.grid = grid
        self.Re = Re
        self.Rei = 1./Re

        # get the imax and jmax quantities used for indexing
        self.imax = grid.imax
        self.jmax = grid.jmax

        self.gamma = 0.0
        self.delke = 0.0
        self.delp =  0.0

    ################### Non Public Interface ###################
    def interpolate_velocity(self):
        pass

    cpdef interpolate_face_velocities(self, double dt):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.grid['ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.grid['vstar'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ustar = grid.grid['Ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Vstar = grid.grid['Vstar'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] ubar = grid.grid['ubar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vbar = grid.grid['vbar'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ubar = grid.grid['Ubar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Vbar = grid.grid['Vbar'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]

        cdef int imax = self.imax
        cdef int jmax = self.jmax
        
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        cdef int i, j

        # compute ubar & vbar (Eq. 4 in Mittal et al.)
        for j in range(1, jmax+1):
            for i in range(1, imax+1):
                ubar[j, i] = ustar[j, i] + 0.5*dt*dxi*( p[j, i+1] - p[j, i-1] )
                vbar[j, i] = vstar[j, i] + 0.5*dt*dyi*( p[j+1, i] - p[j-1, i] )

        # interpolate Ubar
        for j in range(1, jmax+1):
            for i in range(1, imax):
                Ubar[j, i] = 0.5*(ubar[j, i] + ubar[j, i+1])
                
        # interpolate Vbar
        for j in range(1, jmax):
            for i in range(1, imax+1):
                Vbar[j, i] = 0.5*(vbar[j, i] + vbar[j+1, i])

        # compute Ustar
        for j in range(1, jmax+1):
            for i in range(1, imax):
                Ustar[j, i] = Ubar[j, i] - dt*dxi*( p[j, i+1] - p[j, i] )

        # compute Vstar
        for j in range(1, jmax):
            for i in range(1, imax+1):
                Vstar[j, i] = Vbar[j, i] - dt*dyi*( p[j+1, i] - p[j, i] )

    cpdef compute_vstar(self, double dt, double a1, double a2 ):

        # get the underlying grid
        cdef NpyGrid grid = self.grid

        # get the velocity and pressure field variables
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] U = grid.grid['U'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] V = grid.grid['V'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.grid['ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.grid['vstar'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Nx = grid.grid['Nx'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ny = grid.grid['Ny'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Nx1 = grid.grid['Nx1'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ny1 = grid.grid['Ny1'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dx = grid.grid['Dx'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dy = grid.grid['Dy'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dx1 = grid.grid['Dx1'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dy1 = grid.grid['Dy1'][0]

        # locals
        cdef int i, j
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy
        cdef double Rei = self.Rei

        cdef double dxi2 = dxi * dxi
        cdef double dyi2 = dyi * dyi
        cdef double gamma = self.gamma

        # X-Momentum: ustar computed from viscous, convective
        for j in range(1, self.jmax+1):
            for i in range(1, self.imax):

                # Non-linear (convective) terms
                Nx1[j, i] = 0.5*dxi*( (U[j, i+1]*u[j, i+1]) - (U[j, i-1]*u[j, i-1]) )

                Nx1[j, i] += 0.5*dyi*( (V[j+1, i]*u[j+1, i]) - (V[j-1, i]*u[j-1, i]) )

                # Diffisive (viscous) terms
                Dx1[j, i] = Rei*( dxi2*(u[j, i+1] - 2*u[j,i] + u[j, i-1]) + \
                                  dyi2*(u[j+1, i] - 2*u[j,i] + u[j-1, i]) )
                
                # intermediate velocity (ustar)
                ustar[j, i] = u[j, i] + dt*(
                    
                    a1*( Dx1[j, i] - Nx1[j, i] ) - a2*( Dx[j, i] - Nx[j, i] )
                    
                    )

                # Swap arrays for next time step
                Nx[j, i] = Nx1[j, i]
                Dx[j, i] = Dx1[j, i]
                
        # Y-Momentum: vstar computed from viscous, convective
        for j in range(1, self.jmax):
            for i in range(1, self.imax+1):

                # Non-linear (convective) terms
                Ny1[j, i] = 0.5*dxi*( (U[j, i+1]*v[j, i+1]) - (U[j, i-1]*v[j, i-1]) )
                
                Ny1[j, i] += .5*dyi*( (V[j+1, i]*v[j+1, i]) - (V[j-1, i]*v[j-1, i]) )

                # Diffusive (viscous) terms
                Dy1[j, i] = Rei*( dxi2*(v[j, i+1] - 2*v[j,i] + v[j, i-1]) + \
                                  dyi2*(v[j+1, i] - 2*v[j,i] + v[j-1, i]) )


                # Intermediate velocity (vstar)
                vstar[j, i] = v[j, i] + dt*(
                    
                    a1*( Dy1[j, i] - Ny1[j, i] ) - a2*( Dy[j, i] - Ny[j, i] )
                    
                    )

                # Swap arrays for next time step
                Ny[j, i] = Ny1[j, i]
                Dy[j, i] = Dy1[j, i]

    cpdef vstar_boundary_conditions(self):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v0'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.grid['Ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.grid['Vstar'][0]

        cdef int i, j
        
        # Boundary conditions for ustar
        for j in range(1, self.jmax + 1):
            ustar[j, 0] = u[j, 0]
            ustar[j, self.imax] = u[j, self.imax]

        # boundary condition for vstar
        for i in range(1, self.imax+1):
            vstar[0, i] = vstar[0, i]
            vstar[self.jmax, i] = v[self.jmax, i]
                
    cpdef compute_rhs_ppe(self, double dt):
        """ Compute the RHS for the PPE """
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsp = grid.grid['rhsp'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] pold = grid.grid['pold'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] U = grid.grid['Ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] V = grid.grid['Vstar'][0]

        # locals
        cdef int i, j
        cdef double dti = 1./dt
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):
                rhsp[j, i] = dti * ( 0.5*dxi*(U[j, i+1] - U[j, i-1]) + \
                                     0.5*dyi*(V[j+1, i] - V[j-1, i]) )

                # save the old pressure which will be used to check
                # for steady state convergence
                pold[j, i] = p[j, i]
                
    cpdef int solve_ppe_gssor(self):
        """Solve the PPE"""
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhs = grid.grid['rhsp'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] pnew = grid.grid['pnew'][0]

        # locals
        cdef int niter

        # solve the PPE for the updated pressure
        niter = gssor2D(p, pnew, rhs, grid.dx, grid.dy, self.jmax, self.imax, grid.ncells_physical, 1e-8, 10000)
        return niter

    cpdef correct_velocity(self, double dt):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v0'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.grid['ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.grid['vstar'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] U = grid.grid['U'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] V = grid.grid['V'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ustar = grid.grid['Ustar'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Vstar = grid.grid['Vstar'][0]

        # locals
        cdef int i, j
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        # correct u
        for j in range(1, self.jmax+1):
            for i in range(1, self.imax):
                u[j, i] = ustar[j, i] - 0.5*dt*dxi*( p[j, i+1] - p[j, i-1] )
                
        # correct v
        for j in range(1, self.jmax):
            for i in range(1, self.imax+1):
                v[j, i] = vstar[j, i] - 0.5*dt*dyi*( p[j+1, i] - p[j-1, i] )

        # correct U
        for j in range(1, self.jmax+1):
            for i in range(0, self.imax+1):
                U[j, i] = Ustar[j, i] - 0.5*dt*dxi*( p[j, i+1] - p[j, i-1] )
        
        # correct V
        for j in range(0, self.jmax+1):
            for i in range(1, self.imax+1):
                V[j, i] = Vstar[j, i] - 0.5*dt*dyi*( p[j+1, i] - p[j-1, i] )
        

    cpdef compute_stats(self):
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p1 = grid.grid['p'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p2 = grid.grid['pold'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v0'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] vort = grid.grid['vort'][0]

        cdef int i, j
        cdef double delp = 0.0
        cdef double _ke = 0.0

        cdef double dxi = 0.5/grid.dx
        cdef double dyi = 0.5/grid.dy

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):

                # compute change in pressure
                delp = delp + fabs( p1[j, i] - p2[j, i] )

                # sum kinetic energy
                _ke = _ke + 0.5 * ( u[j, i]*u[j, i] + v[j, i]*v[j, i] )

                # vorticity
                vort[j, i] = dyi * (u[j+1, i] - u[j-1, i]) - dxi * (v[j, i+1] - v[j, i-1])

        self.delp = delp
        self.delke = _ke - self.ke
    
        self.ke = _ke

cdef class EDAC2D(CollocatedGrid2D):
    def set_mach_number(self, double Ma):
        self.Ma = Ma
        self.Mai = 1./Ma

    cpdef compute_rhs(self, double dt, int stage=0, bint penalty_ibm=False):
        cdef NpyGrid grid = self.grid

        # field variables
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p%d'%stage][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] uc = grid.grid['uc'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vc = grid.grid['vc'][0]

        # rhs terms
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsu = grid.grid['rhsu'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsv = grid.grid['rhsv'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsp = grid.grid['rhsp'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] lapu = grid.grid['lapu'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] lapv = grid.grid['lapv'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] lapp = grid.grid['lapp'][0]
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] div = grid.grid['div'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] divvx = grid.grid['divvx'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] divvy = grid.grid['divvy'][0]
        
        #cdef np.ndarray[ndim=2, dtype=np.float64_t] px = grid.grid['px'][0]
        #cdef np.ndarray[ndim=2, dtype=np.float64_t] py = grid.grid['py'][0]

        cdef np.ndarray[ndim=2, dtype=np.int32_t] mapping = grid.grid['mapping'][0]

        cdef int imax = self.imax
        cdef int jmax = self.jmax

        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        cdef double Rei = self.Rei
        cdef double Mai = self.Mai

        cdef double gamma = 0.9
        cdef double large = 0.0

        # locals
        cdef int i, j

        # Compute source terms
        self._laplacian(p, lapp)
        self._laplacian(u, lapu)
        self._laplacian(v, lapv)
        
        self._divergence(u, v, div)
        self._gradient(div, divvx, divvy)

        if penalty_ibm:
            large = 1e6

        for j in range(1, jmax+1):
            for i in range(1, imax+1):

                #################### U-Momentum equation ####################

                # Convective terms
                rhsu[j, i]  = -dxi*( 0.25*(u[j, i] + u[j, i+1])*(u[j, i] + u[j, i+1]) -\
                                     0.25*(u[j, i] + u[j, i-1])*(u[j, i] + u[j, i-1]) )

                rhsu[j, i] += -dyi*( 0.25*(u[j, i] + u[j+1, i])*(v[j, i] + v[j+1, i]) -\
                                     0.25*(u[j, i] + u[j-1, i])*(v[j, i] + v[j-1, i]) )

                # Pressure gradient
                rhsu[j, i] += -dxi*( 0.5*(p[j, i]+p[j, i+1]) - 0.5*(p[j, i]+p[j, i-1]) )

                # Source terms
                rhsu[j, i] += Rei/3.0 * divvx[j, i]
                rhsu[j, i] += u[j, i]*div[j, i] + Rei*lapu[j, i]

                # penalty IBM treatment (this will only affect solid cells)
                rhsu[j, i] -= large*u[j, i]*mapping[j, i]

                #################### V-Momentum equation ####################
                
                # Convective terms
                rhsv[j, i]  = -dxi*( 0.25*(u[j, i] + u[j, i+1])*(v[j, i] + v[j, i+1]) -\
                                     0.25*(u[j, i] + u[j, i-1])*(v[j, i] + v[j, i-1]) )

                rhsv[j, i] += -dyi*( 0.25*(v[j, i] + v[j+1, i])*(v[j, i] + v[j+1, i]) -\
                                     0.25*(v[j, i] + v[j-1, i])*(v[j, i] + v[j-1, i]) )

                # Pressure gradient
                rhsv[j, i] += -dyi*( 0.5*(p[j, i]+p[j+1, i]) - 0.5*(p[j, i]+p[j-1, i]) )

                # Source terms
                rhsv[j, i] += Rei/3.0 * divvy[j, i]
                rhsv[j, i] += v[j, i]*div[j, i] + Rei*lapv[j, i]

                # penalty IBM treatment (this will only affect solid cells)
                rhsu[j, i] -= large*v[j, i]*mapping[j, i]

                #################### Pressure equation ####################
                
                # Convective terms
                rhsp[j, i]  = -dxi*( 0.25*(p[j, i]+p[j, i+1])*(u[j, i]+u[j, i+1]) -\
                                     0.25*(p[j, i]+p[j, i-1])*(u[j, i]+u[j, i-1]) )

                rhsp[j, i] += -dyi*( 0.25*(p[j, i]+p[j+1, i])*(v[j, i]+v[j+1, i]) -\
                                     0.25*(p[j, i]+p[j-1, i])*(v[j, i]+v[j-1, i]) )

                # Source terms
                rhsp[j, i] += div[j, i] * (2*p[j, i] - Mai*Mai) + Rei*lapp[j, i]

    cpdef update(self, double dt, int stage, int nstages=1):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsu = grid.grid['rhsu'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsv = grid.grid['rhsv'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsp = grid.grid['rhsp'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u0 = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] us = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] un = grid.grid['u%d'%((stage+1)%nstages)][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] v0 = grid.grid['v0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vs = grid.grid['v%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vn = grid.grid['v%d'%((stage+1)%nstages)][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p0 = grid.grid['p0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] ps = grid.grid['p%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] pn = grid.grid['p%d'%((stage+1)%nstages)][0]

        cdef np.ndarray[ndim=2, dtype=np.int32_t] mapping = grid.grid['mapping'][0]

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):
                
                if stage == 0:
                    un[j, i] = (u0[j, i] + dt * rhsu[j, i])*(1.0 - mapping[j, i])
                    vn[j, i] = (v0[j, i] + dt * rhsv[j, i])*(1.0 - mapping[j, i])
                    pn[j, i] = p0[j, i] + dt * rhsp[j, i]

                elif stage == 1:
                    un[j, i] = (0.75*u0[j, i] + 0.25*(us[j, i] + dt*rhsu[j,i]))*(1.0 - mapping[j, i])
                    vn[j, i] = (0.75*v0[j, i] + 0.25*(vs[j, i] + dt*rhsv[j,i]))*(1.0 - mapping[j, i])
                    pn[j, i] = 0.75*p0[j, i] + 0.25*(ps[j, i] + dt*rhsp[j,i])

                elif stage == 2:
                    un[j, i] = (1./3*u0[j, i] + 2./3*(us[j, i] + dt*rhsu[j,i]))*(1.0 - mapping[j, i])
                    vn[j, i] = (1./3*v0[j, i] + 2./3*(vs[j, i] + dt*rhsv[j,i]))*(1.0 - mapping[j, i])
                    pn[j, i] = 1./3*p0[j, i] + 2./3*(ps[j, i] + dt*rhsp[j,i])

cdef class SGPCVanKannIntegrator2D:
    """Pressure correction scheme on a staggered grid with Van Kann
    type discretization in two dimensions.

    The Van Kann scheme uses an Adams bash forth for the advection and
    diffusion terms.

    This pressure correction scheme with immersed boundaries is:
      
        - predict v* using AB scheme
        - augment v* with the ibm force
        - set any boundary conditions on v*
        - setup the ppe coeff matrix and rhs
        - solve the ppe to get pressure correction
        - correct pressure and velocity

    Notes:
    
    In the prediction stage, we use an Adams Bashforth scheme for the
    advection and diffusive terms and a standard difference for the
    pressure term. 

    The AB scheme requires the discretization for the advection and
    diffusion terms to be available for two time levels (n and
    n-1).

    The grid is is defined by nrow X ncol cells (including two ghost
    cells). A cell in the staggered grid looks like:

    ---------*--------
    |                |
    |                |
    |                |
    |       o        x
    |                |
    |                |      o -- pressure node
    |                |      x -- u-velocity node
    ------------------      * -- v-velocity node
    
    We assume our domain boundaries are axis aligned. Due to the
    staggered arrangement of the variables on the cell, we have the
    following indexing convention for the velocity variables:
    
    u velocity: index range [0:nrow-1, 0:ncol-1]
    [:, 0]         -- physical nodes along the left boundary
    [:, ncol-2]    -- physical nodes along the right boundary
    [:, ncol-1]    -- unused values
    [0, :]         -- ghost nodes along the bottom boundary
    [nrow-1, :]    -- ghost nodes along the top boundary
    
    v velocity: index range [0:nrow-1, 0:ncol-1]
    [:, 0]         -- ghost nodes along the left boundary
    [:, ncol-1]    -- ghost nodes along the right boundary
    [0, :]         -- physical nodes along the bottom boundary
    [nrow-2, :]    -- physical nodes along the top boundary
    [nrow-1, :]    -- unused nodes
    
    The arrangement of the variables imply we need one less cell along
    the 'x' direction for the u-velocity and one less cell along the
    'y' direction for the v-velocity. Boundary conditions for the
    velocity are directly imposed on the velocity nodes that lie on
    the boundary.

    """
    def __init__(
        self, NpyGrid grid, int boundary_condition, double uinflow,
        bint moving_boundary=False):
        """Constructor"""
        self.grid = grid
        self.boundary_condition = boundary_condition
        self.uinflow = uinflow
        
        self.moving_boundary = moving_boundary

        # initialize the coefficient martrix and rhs for the PPE. The
        # size of these arrays are determined by the number of
        # physical cells in the grid
        ncells_physical = grid.ncells_physical
        ncells = grid.ncells
        self.coeffmat_ppe = sparse.lil_matrix( (ncells_physical, ncells_physical) )
        self.rhs_ppe = np.zeros( shape=(ncells_physical,) )

        # create the coefficient matrix in the beginning
        self.set_coeffmat_ppe()

    cpdef get_vstar(self, double Re, double dt, int iteration):
        """2D discretization for the pressure correction scheme on staggered grids
        to compute the predicted velocities.

        Parameters:

        Re : double 
            Reynolds number for the problem

        dt : double
            Time step

        iteration : int
            Iteration counter

        Notes:

        The predicted velocity is calculated using an Adams Bashforth
        scheme for the advecton and diffusion terms and a centered
        difference for the pressure term. The AB scheme requires the
        discretized right-hand-side. We use two field variables 'rhsx',
        and 'rhsy' to store the discretizations at the previous time
        levels.

        """
        # the grid for the calculation
        cdef NpyGrid grid = self.grid

        # number of physical cells along the row and column directions
        cdef int nrows = grid.nrows
        cdef int ncols = grid.ncols

        # grid constants and Reynolds number
        cdef double dx1 = 1./grid.dx, dy1 = 1./grid.dy, Re1 = 1./Re

        # field variables.
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.get_field('u')[0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsx = grid.get_field('rhsx')[0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.get_field('v')[0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsy = grid.get_field('rhsy')[0]

        # predicted velocities
        cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.get_field('ustar')[0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.get_field('vstar')[0]

        # pressure
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.get_field('p')[0]

        # locals variables
        cdef Py_ssize_t i, j
        cdef double a1, a2, a3, a4, a5, a6
        cdef double rhsxij, rhsyij
        cdef double c1 = 1.5, c2 = -0.5

        # coefficients for the AB scheme. For the first iteration, we
        # use Euler integration which can be selected with a change of
        # coefficients.
        if iteration == 0:
            c1 = 1.0; c2 = 0.0

        # loop over to define rhsx at this level and update ustar
        for i in range(1, nrows-1):
            for j in range(1, ncols-2):
                a1 = 0.5 * ( u[i, j+1] + u[i, j] )
                a2 = 0.5 * ( u[i, j-1] + u[i, j] )
                a3 = 0.5 * ( u[i+1, j] + u[i, j] )
                a4 = 0.5 * ( u[i-1, j] + u[i, j] )
                a5 = 0.5 * ( v[i, j+1] + v[i, j] )
                a6 = 0.5 * ( v[i-1, j+1] + v[i-1, j] )

                # convective terms
                rhsxij = -( ((a1*a1)-(a2*a2))*dx1 + ((a3*a5)-(a4*a6))*dy1 )

                # diffisive terms
                rhsxij = rhsxij + \
                    Re1* ( (u[i, j+1] - 2*u[i, j] + u[i, j-1])*dx1*dx1 + \
                               (u[i-1, j] - 2*u[i, j] + u[i+1, j])*dy1*dy1 )

                # update ustar with gradient of pressure
                ustar[i, j] = u[i,j] - dt*dx1 * ( p[i,j+1] - p[i,j] )

                # update ustar with advection and diffusion terms
                ustar[i, j] = ustar[i,j] + dt * \
                    ( c1*rhsxij - c2*rhsx[i,j] )

                # store the current rhsxij for the next time level
                rhsx[i, j] = rhsxij

        # loop over to define rhsy and update vstar
        for i in range(1, nrows-2):
            for j in range(ncols-1):
                a1 = 0.5 * ( u[i+1, j] + u[i, j] )
                a2 = 0.5 * ( v[i, j+1] + v[i, j] )
                a3 = 0.5 * ( u[i+1, j-1] + u[i, j-1] )
                a4 = 0.5 * ( v[i, j-1] + v[i, j] )
                a5 = 0.5 * ( v[i+1, j] + v[i, j] )
                a6 = 0.5 * ( v[i-1, j] + v[i, j] )

                # convective terms
                rhsyij = -( ((a1*a2)-(a3*a4))*dx1 + ((a5*a5)-(a6*a6))*dy1 )

                # diffusive terms
                rhsyij = rhsyij + \
                    Re1*( (v[i, j+1] - 2*v[i, j] + v[i, j-1])*dx1*dx1 + \
                              (v[i-1, j] -2*v[i-1, j] + v[i+1, j])*dy1*dy1 )

                # update vstar with gradient of pressure
                vstar[i, j] = v[i, j] - dt*dy1 * ( p[i+1, j] - p[i, j] )

                # update vstar with advection and diffusion terms
                vstar[i, j] = vstar[i, j] + dt * (c1*rhsyij - c2*rhsy[i,j])

                # store the rhsy value for the next time level
                rhsy[i, j] = rhsyij

    cpdef set_boundary_vstar(self):
        """Set the boundary conditions for the predicted velocities

        We assume our domain boundaries are axis aligned. Due to the
        staggered arrangement of the variables on the cell, we have the
        following indexing convention for the velocity variables:
        
        u velocity:
        [:, 0]         -- physical nodes along the left boundary
        [:, ncols-2]   -- physical nodes along the right boundary
        [:, ncols-1]   -- unused values
        [0, :]         -- ghost nodes along the bottom boundary
        [nrows-1, :]   -- ghost nodes along the top boundary
        
        v velocity:
        [:, 0]         -- ghost nodes along the left boundary
        [:, ncols-1]   -- ghost nodes along the right boundary
        [0, :]         -- physical nodes along the bottom boundary
        [nrows-2, :]   -- physical nodes along the top boundary
        [nrows-1, :]   -- unused nodes
        
        """
        cdef int boundary_condition = self.boundary_condition

        cdef double uinflow = self.uinflow
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.get_field('ustar')[0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.get_field('vstar')[0]

        cdef int nrows = grid.nrows
        cdef int ncols = grid.ncols
        
        # LDC. Direchlet boundary conditions everywhere
        if boundary_condition == 1:
            # bottom
            u[0, :] = -u[1, :]
            v[0, :] = 0.

            # top
            u[nrows-1, :] = 2*uinflow - u[nrows-2, :]
            v[nrows-1, :] = -v[nrows-2, :]

            # left
            u[:, 0] = 0.
            v[:, 0] = -v[:, 1]

            # right
            u[:, ncols-2] = 0.       
            v[:, ncols-1] = -v[:, ncols-2] 
            
        else: 
            # left
            u[:, 0] = uinflow                     # inflow velocity
            v[:, 0] = v[:, 1]                     # dv/dx = 0
        
            # right
            u[:, ncols-2] = u[:, ncols-3]         # du/dx = 0
            v[:, ncols-1] = v[:, ncols-2]         # dv/dx = 0

            # bottom
            u[0, :] = u[1, :]                     # du/dy = 0
            v[0, :] = 0.0                         # v = 0

            # top
            u[nrows-1, :] = u[nrows-2, :]         # du/dy = 0
            v[nrows-2, :] = 0                     # v = 0

    cpdef set_rhs_ppe(self, double dt):
        """Setup the coefficient matrix for the Pressure Poisson Equation """
        cdef NpyGrid grid = self.grid
        cdef int nrows = grid.nrows
        cdef int ncols = grid.ncols

        cdef double dtdx1 = 1./(dt * grid.dx)
        cdef double dtdy1 = 1./(dt * grid.dy)

        # rhs vector for ppe
        cdef np.ndarray[ndim=1, dtype=np.float64_t] rhs_ppe = self.rhs_ppe

        # grid field variables
        cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.get_field('ustar')[0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.get_field('vstar')[0]

        # locals
        cdef Py_ssize_t i, j, index = 0

        # iterate over each internal cell and set the rhs for the ppe
        for i in range(1, nrows-1):
            for j in range(1, ncols-1):

                # right-hand-side for the ppe
                rhs_ppe[index] = dtdx1 * (ustar[i, j] - ustar[i, j-1]) + \
                    dtdy1 * (vstar[i, j] - vstar[i-1, j])

                index = index + 1

        # Additionally, set-up coeff mat if using moving boundaries
        if self.moving_boundary:
            self.set_coeffmat_ppe()

    def set_coeffmat_ppe(self):
        if self.boundary_condition == 1:
            self._coeffmat_ldc()
        else:
            self._coeffmat_inflow()

    cpdef solve_ppe(self):
        """Solve the Pressure Poisson Equation"""
        cdef NpyGrid grid = self.grid
        cdef np.ndarray[ndim=1, dtype=np.float64_t] rhs = self.rhs_ppe
        cdef np.ndarray[ndim=2, dtype=np.float64_t] pcorr = grid.get_field('pcorr')[0]

        cdef int nrows = grid.nrows
        cdef int ncols = grid.ncols

        cdef int boundary_condition = self.boundary_condition

        # Locals
        cdef Py_ssize_t i, j, index=0
        
        # solve the system using the GMRES algorithm using SciPy's
        # sparse solvers toolkit
        sol, info = linalg.gmres(self.coeffmat_ppe, rhs)

        # put the solution into the pcorr vector
        for i in range(1, nrows-1):
            for j in range(1, ncols-1):
                pcorr[i, j] = sol[index]
                
                index = index + 1
                
        # impose boundary conditions on the pressure correction vector
        if boundary_condition == 1: # LDC: dp/dn = 0
            for j in range(ncols):
                pcorr[0, j] = pcorr[1, j]
                pcorr[nrows-1, j] = pcorr[nrows-2, j]

            for i in range(nrows):
                pcorr[i, 0] = pcorr[i, 1]
                pcorr[i, ncols-1] = pcorr[i, ncols-2]
            
        else: # Flow past something
            for j in range(ncols):
                pcorr[0, j] = pcorr[1, j]             # dp/dy = 0 along bottom edge
                pcorr[nrows-1, j] = pcorr[nrows-2, j] # dp/dy = 0 along top edge

            for i in range(nrows):
                pcorr[i, 0] = pcorr[i, 1]             # dp/dx = 0 along left edge
                pcorr[i, ncols-1] = -pcorr[i,ncols-2] # p = 0 along right edge

    cpdef update_pv(self, double dt):
       "Update the pressure and velocity from the pressure correction vectors"
       cdef NpyGrid grid = self.grid
       cdef int nrows = grid.nrows
       cdef int ncols = grid.ncols

       cdef double dtdxi = 1./(dt * grid.dx)
       cdef double dtdyi = 1./(dt * grid.dy)

       cdef double uinflow = self.uinflow
       cdef int boundary_condition = self.boundary_condition

       # field variables
       cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.get_field('p')[0]
       cdef np.ndarray[ndim=2, dtype=np.float64_t] pcorr = grid.get_field('pcorr')[0]

       cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.get_field('u')[0]
       cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.get_field('v')[0]
       
       cdef np.ndarray[ndim=2, dtype=np.float64_t] ustar = grid.get_field('ustar')[0]
       cdef np.ndarray[ndim=2, dtype=np.float64_t] vstar = grid.get_field('vstar')[0]
       
       # locals
       cdef Py_ssize_t i, j
       
       ### UPDATE PRESSURE AT THE INTERIOR NODES ###
       for i in range( 1, nrows-1 ):
           for j in range( 1, ncols-1 ):
               p[i, j] = p[i, j] + pcorr[i, j]

       ### BOUNDARY CONDITIONS FOR THE UPDATED PRESSURE ###
       if boundary_condition == 1: # LDC
           for i in range(nrows):
               p[i, 0] = p[i, 1]                      # dp/dx = 0
               p[i, ncols-1] = p[i, ncols-2]          # dp/dx = 0

           for j in range(ncols):
               p[0, j] = p[1, j]                      # dp/dy = 0
               p[nrows-1, j] = p[nrows-2, j]          # dp/dy = 0

       else: # Flow past something
           for i in range(nrows):
               p[i, 0] = p[i, 1]                       # left dp/dx = 0
               p[i, ncols-1] = -p[i, ncols-2]          # riht p = 0

           for j in range(ncols):
               p[0, j] = p[1, j]                       # bottom dp/dy = 0
               p[nrows-1, j] = p[nrows-2, j]           # top dp/dy = 0

       ### UPDATE U FOR INTERIOR NODES ###
       for i in range(1, nrows-1):
           for j in range(1, ncols-2):
               u[i, j] = ustar[i, j] - (pcorr[i, j+1] - pcorr[i, j])*dtdxi

       ### UPDATE V FOR INTERIOR NODES ###
       for i in range(1, nrows-2):
           for j in range(1, ncols-1):
               v[i, j] = vstar[i, j] - (pcorr[i+1, j] - pcorr[i, j])*dtdxi
               
       ### BOUNDARY CONDITIONS FOR UPDATED VELOCITIES
       if boundary_condition == 1: # LDC
           for i in range(nrows):
               u[i, 0] = 0.                             # u = 0 along left
               u[ncols-2, 0] = 0.                       # u = 0 along right

               v[i, 0] = -v[i, 1]                       # v = 0 along left
               v[i, ncols-1] = -v[i, ncols-2]           # v = 0 along right

           for j in range(ncols):
               u[0, j] = -u[1, j]                        # u = 0 along bottom
               u[nrows-1, j] = 2*uinflow - u[nrows-2, j] # u = U along top

               v[0, j] = 0.                             # v = 0 along bottom
               v[nrows-2, j] = 0.                       # v = 0 along top

       else: # Flow past something
           for i in range(nrows):
               u[i, 0] = uinflow                                 # uinflow along left edge
               u[i, ncols-2] = ustar[i, ncols-2] - \
                   (pcorr[i, ncols-1] - pcorr[i, ncols-2])*dtdxi # u from continuity on right edge


               v[i, 0] = v[i, 1]                                 # dv/dx = 0 along left edge
               v[i, ncols-1] = v[i, ncols-2]                     # dv/dx = 0 along right edge

           for j in range(ncols):
               u[0, j] = u[1, j]                                 # du/dy = 0 along bottom
               u[nrows-1, j] = u[nrows-2, j]                     # du/dy = 0 along top

               v[0, j] = 0.                                      # v = 0 along bottom
               v[nrows-2, j] = 0.                                # v = 0 along top

    cpdef add_ibm_force(self, double dt):
        """Add the IBM force to the predicted velocities"""
        pass

    cpdef _coeffmat_inflow(self):
        """Setup the coefficient matrix for flow past an object"""
        cdef NpyGrid grid = self.grid
        cdef int nrows = grid.nrows
        cdef int ncols = grid.ncols        

        cdef double dxi2 = 1./(grid.dx * grid.dx)
        cdef double dyi2 = 1./(grid.dy * grid.dy)

        # coefficient matrix for ppe
        mat = self.coeffmat_ppe

        # locals
        cdef Py_ssize_t indexi, indexj
        cdef Py_ssize_t row=-1, jump=ncols-2
        
        # iterate over the cells and set the coefficient matrix
        for indexi in range(1, nrows-1):
            for indexj in range(1, ncols-1):

                # the current cell
                row = row + 1

                # bottom left corner
                if ( (indexi==1) and (indexj==1) ):
                    mat[row, row] = -(dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row+jump] = dyi2

                # bottom right corner
                elif ( (indexi==1) and (indexj == (ncols-2)) ):
                    mat[row, row] = -(3*dxi2 + dyi2)
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2

                # top left corner
                elif ( (indexi==(nrows-2)) and (indexj == 1) ):
                    mat[row, row] = -(dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-jump] = dyi2

                # top right corner
                elif ( (indexi==(nrows-2)) and (indexj==(ncols-2)) ):
                    mat[row, row] = -(3*dxi2 + dyi2)
                    mat[row, row-1] = dxi2
                    mat[row, row-jump] = dyi2

                # bottom edge
                elif ( indexi == 1 ):
                    mat[row, row] = -(2*dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2

                # top edge
                elif ( indexi == (nrows-2) ):
                    mat[row, row] = -(2*dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-1] = dxi2
                    mat[row, row-jump] = dyi2

                # left edge
                elif ( indexj == 1 ):
                    mat[row, row] = -(dxi2 + 2*dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row+jump] = dyi2
                    mat[row, row-jump] = dyi2

                # right edge
                elif ( indexj==(ncols-2) ):
                    mat[row, row] = -(3*dxi2 + 2*dyi2)
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2
                    mat[row, row-jump] = dyi2

                # interior cells
                else:
                    mat[row, row] = -2*(dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2
                    mat[row, row-jump] = dyi2

        # convert the matrix to csr format
        mat = mat.tocsr()
        self.coeffmat_ppe = mat

    cpdef _coeffmat_ldc(self):
        """Setup the coefficient matrix for the LDC problem"""
        cdef NpyGrid grid = self.grid
        cdef int nrows = grid.nrows
        cdef int ncols = grid.ncols        

        cdef double dxi2 = 1./(grid.dx * grid.dx)
        cdef double dyi2 = 1./(grid.dy * grid.dy)

        # coefficient matrix for ppe
        mat = self.coeffmat_ppe

        # locals
        cdef Py_ssize_t indexi, indexj
        cdef Py_ssize_t row=-1, jump=ncols-2
        
        # iterate over the cells and set the coefficient matrix
        for indexi in range(1, nrows-1):
            for indexj in range(1, ncols-1):

                # increment for each cell
                row = row + 1

                # bottom left corner
                if ( (indexi==1) and (indexj==1) ):
                    mat[row, row] = -(dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row+jump] = dyi2

                # bottom right corner
                elif ( (indexi==1) and (indexj == (ncols-2)) ):
                    mat[row, row] = -(dxi2 + dyi2)
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2

                # top left corner
                elif ( (indexi==(nrows-2)) and (indexj == 1) ):
                    mat[row, row] = -(dxi2 + 3*dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-jump] = dyi2

                # top right corner
                elif ( (indexi==(nrows-2)) and (indexj==(ncols-2)) ):
                    mat[row, row] = -(dxi2 + 3*dyi2)
                    mat[row, row-1] = dxi2
                    mat[row, row-jump] = dyi2

                # bottom edge
                elif ( indexi == 1 ):
                    mat[row, row] = -(2*dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2

                # top edge
                elif ( indexi == (nrows-2) ):
                    mat[row, row] = -(2*dxi2 + 3*dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-1] = dxi2
                    mat[row, row-jump] = dyi2

                # left edge
                elif ( indexj == 1 ):
                    mat[row, row] = -(dxi2 + 2*dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row+jump] = dyi2
                    mat[row, row-jump] = dyi2

                # right edge
                elif ( indexj==(ncols-2) ):
                    mat[row, row] = -(dxi2 + 2*dyi2)
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2
                    mat[row, row-jump] = dyi2

                # interior cells
                else:
                    mat[row, row] = -2*(dxi2 + dyi2)
                    mat[row, row+1] = dxi2
                    mat[row, row-1] = dxi2
                    mat[row, row+jump] = dyi2
                    mat[row, row-jump] = dyi2

        # convert the matrix to csr format
        mat = mat.tocsr()
        self.coeffmat_ppe = mat
