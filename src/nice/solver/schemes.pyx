import numpy as np
cimport numpy as np

from libc.math cimport fabs, sqrt, fmax
cimport cython

# Poisson solvers
from poisson cimport gssor2D

cdef class SGPressureCorrection2D:
    """Implementation of a Pressure projection scheme for the
    Incompressible NS equations on a Staggered grid as shown below

    ---------*--------
    |                |
    |                |
    |                |
    |       o        x
    |                |
    |                |      o -- pressure node
    |                |      x -- u-velocity node
    ------------------      * -- v-velocity node

    The discretization follows the book "Numerical Simulation in Fluid
    Dynamics: A Practical Introduction".

    In the 2D case, the grid is defined to have "imax+1" cells along
    the "X" direction and "jmax+1" cells along the "Y" direction. Due
    to the staggered arrangement of the variables, the u-velocity
    nodes have one less index in the 'X' direction and the v-velocity
    nodes have one less index in the 'Y' direction. 

    The boundary nodes along the faces are given as:

    [j, 0]    for j = 1, ..., jmax  ---- West
    [j, imax] for j = 1, ..., jmax  ---- East

    [0, i]    for i = 1, ..., imax  ---- South
    [jmax, i] for i = 1, ..., imax  ---- North

    """
    def __init__(self, NpyGrid grid, double Re=1000.0, double gamma=0.0):
        """Constructor for the scheme
        
        Parameters:
        -----------
        
        grid : NpyGrid
            Underlying grid block on which the equations are solved

        Re : double
            Reynolds number for the simulation

        gamma : double
            Constant for the donor cell discretization
        
        """
        self.grid = grid
        self.Re = Re
        self.Rei = 1./Re

        # get the imax and jmax quantities used for indexing
        self.imax = grid.imax
        self.jmax = grid.jmax

        self.gamma = gamma

        self.delke = 0.0
        self.delp =  0.0

    def set_donor_cell_discretization_gamma(self, double gamma):
        """Set the constant [0, 1] for the donor cell discretization"""
        self.gamma = gamma

    ################### Non Public Interface ###################
    cpdef compute_vstar(self, double dt, double a1, double a2, int stage=0, bint penalty_ibm=False):
        """Compute the intermediate (non divergence free) velocity

        Parameters:
        
        dt : double
            Current time-step

        a1, a2 : Backward difference coefficients

        stage : int
            Integrator stage for the case of a multi-stage integration

        """
        # get the underlying grid
        cdef NpyGrid grid = self.grid

        # get the velocity and pressure field variables
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] F = grid.grid['F'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] G = grid.grid['G'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Nx = grid.grid['Nx'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ny = grid.grid['Ny'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Nx1 = grid.grid['Nx1'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Ny1 = grid.grid['Ny1'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dx = grid.grid['Dx'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dy = grid.grid['Dy'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dx1 = grid.grid['Dx1'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] Dy1 = grid.grid['Dy1'][0]

        cdef np.ndarray[ndim=2, dtype=np.int32_t] mapu = grid.grid['mapu'][0]
        cdef np.ndarray[ndim=2, dtype=np.int32_t] mapv = grid.grid['mapv'][0]

        # locals
        cdef int i, j
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy
        cdef double Rei = self.Rei

        cdef double dxi2 = dxi * dxi
        cdef double dyi2 = dyi * dyi
        cdef double gamma = self.gamma

        cdef double large = 0.0
        if penalty_ibm:
            large = 1e6

        # X-Momentum: ustar (F) computed from viscous, convective and
        # donor cell discretization, discretized at the center of the
        # physical vertical faces.
        for j in range(1, self.jmax+1):
            for i in range(1, self.imax):

                # Non-linear (convective) terms
                Nx1[j, i] = dxi*( 
                    0.25*(u[j, i] + u[j, i+1])*(u[j, i] + u[j, i+1]) - \
                    0.25*(u[j, i-1] + u[j, i])*(u[j, i-1] + u[j, i]) + \
                        gamma*( 0.25*fabs(u[j,i] + u[j,i+1]) * (u[j,i] - u[j,i+1]) -\
                                0.25*fabs(u[j,i-1]+u[j,i]) * (u[j,i-1] - u[j,i]) )
                                
                    ) +  dyi*( 
                    0.25*(v[j, i] + v[j, i+1])*(u[j, i] + u[j+1, i]) - \
                    0.25*(v[j-1, i] + v[j-1, i+1])*(u[j-1, i] + u[j, i]) +\
                        gamma*( 0.25*fabs(v[j,i] + v[j,i+1]) * (u[j,i] - u[j+1, i]) -\
                                0.25*fabs(v[j-1, i] + v[j-1, i+1]) * (u[j-1,i] - u[j,i]) )

                    ) +large*u[j, i]*mapu[j, i]
                
                # Diffisive (viscous) terms
                Dx1[j, i] = Rei*( dxi2*(u[j, i+1] - 2*u[j,i] + u[j, i-1]) + \
                                  dyi2*(u[j+1, i] - 2*u[j,i] + u[j-1, i]) )

                # intermediate velocity ustar
                F[j, i] = (u[j, i] + dt*(
                    a1*( Dx1[j, i] - Nx1[j, i] ) - a2*( Dx[j, i] - Nx[j, i] )))*(1.0-mapu[j, i])

                # Swap arrays for next time step
                Nx[j, i] = Nx1[j, i]
                Dx[j, i] = Dx1[j, i]
                
        # Y-Momentum: vstar (G) computed from viscous, convective and
        # donor cell discretization, discretized at the center of the
        # horizontal faces
        for j in range(1, self.jmax):
            for i in range(1, self.imax+1):

                # Non-linear (convective) terms
                Ny1[j, i] = dxi*( 
                    0.25*(v[j, i] + v[j, i+1])*(u[j, i] + u[j+1, i]) -\
                    0.25*(u[j, i-1] + u[j+1, i-1])*(v[j, i-1] + v[j, i]) +\
                    gamma*( 0.25*fabs(u[j,i] + u[j+1,i]) * (v[j, i] - v[j, i+1]) -\
                            0.25*fabs(u[j, i-1] + u[j+1, i-1]) * (v[j, i-1] - v[j, i]) )
                ) + dyi*( 
                    0.25*(v[j, i] + v[j+1, i])*(v[j, i] + v[j+1, i]) - \
                    0.25*(v[j-1, i] + v[j, i])*(v[j-1, i] + v[j, i]) +\
                    gamma*( 0.25*fabs(v[j,i] + v[j+1, i]) * (v[j, i] - v[j+1, i]) -\
                            0.25*fabs(v[j-1, i] + v[j, i]) * (v[j-1, i] - v[j, i]) )

                ) +large*v[j, i]*mapv[j, i]

                # Diffusive (viscous) terms
                Dy1[j, i] = Rei*( dxi2*(v[j, i+1] - 2*v[j,i] + v[j, i-1]) + \
                                  dyi2*(v[j+1, i] - 2*v[j,i] + v[j-1, i]) )

                # Intermediate velocity (vstar)
                G[j, i] = (v[j, i] + dt*(
                    a1*( Dy1[j, i] - Ny1[j, i] ) - a2*( Dy[j, i] - Ny[j, i] )))*(1.0-mapv[j,i])

                # Swap arrays for next time step
                Ny[j, i] = Ny1[j, i]
                Dy[j, i] = Dy1[j, i]

    cpdef vstar_boundary_conditions(self, int stage=0):
        """Apply boundary conditions to the intermediate velocities"""      
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] F = grid.grid['F'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] G = grid.grid['G'][0]

        cdef int i, j
        
        # Boundary conditions for ustar (F).
        for j in range(1, self.jmax + 1):
            F[j, 0] = u[j, 0]
            F[j, self.imax] = u[j, self.imax]

        # boundary condition for vstar (G)
        for i in range(1, self.imax+1):
            G[0, i] = v[0, i]
            G[self.jmax, i] = v[self.jmax, i]            
                
    cpdef compute_rhs_ppe(self, double dt):
        """ Compute the RHS for the PPE """
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhsp = grid.grid['rhsp'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] pold = grid.grid['pold'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] F = grid.grid['F'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] G = grid.grid['G'][0]

        # locals
        cdef int i, j
        cdef double dti = 1./dt
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):
                rhsp[j, i] = dti * ( dxi*(F[j, i] - F[j, i-1]) + \
                                     dyi*(G[j, i] - G[j-1, i]) )

                # save the old pressure which will be used to check
                # for steady state convergence
                pold[j, i] = p[j, i]
                
    cpdef solve_ppe_gssor(self, int max_iter=100000, bint outlet_pressure_dirichlet=False):
        """Solve the PPE using the GSSOR iteration"""
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] rhs = grid.grid['rhsp'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] pnew = grid.grid['pnew'][0]

        # solve the PPE for the updated pressure
        cdef int niter
        cdef double omega = 1.7
        cdef double tol = 1e-8

        niter = gssor2D(p, pnew, rhs, grid.dx, grid.dy, self.jmax, self.imax, grid.ncells_physical,
                        tol, max_iter, omega, outlet_pressure_dirichlet)

        self.niter = niter

    cpdef correct_velocity(self, double dt, int stage=0):
        """Correct the velocity with the gradient of the corrected pressure """
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] p = grid.grid['p'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] F = grid.grid['F'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] G = grid.grid['G'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] div = grid.grid['div'][0]

        # locals
        cdef int i, j
        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        cdef double max_div = -1e100

        # correct u
        for j in range(1, self.jmax+1):
            for i in range(1, self.imax):
                u[j, i] = F[j, i] - dt*dxi*( p[j, i+1] - p[j, i] )
                
        # correct v
        for j in range(1, self.jmax):
            for i in range(1, self.imax+1):
                v[j, i] = G[j, i] - dt*dyi*( p[j+1, i] - p[j, i] )

        # compute divergence
        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):
                div[j, i] = dxi*(u[j,i]-u[j,i-1]) + dyi*(v[j,i]-v[j-1,i])
                max_div = fmax(max_div, div[j,i])

        # store maximum divergence
        self.max_div = max_div

    cpdef compute_stats(self, int stage=0):
        cdef NpyGrid grid = self.grid
        
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p1 = grid.grid['p'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] p2 = grid.grid['pold'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] uc = grid.grid['uc'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vc = grid.grid['vc'][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] vort = grid.grid['vort'][0]    

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u0'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v0'][0]

        cdef int i, j
        cdef double delp = 0.0
        cdef double _ke = 0.0

        cdef double dxi = 1./grid.dx
        cdef double dyi = 1./grid.dy

        cdef double dx2i = 0.5*dxi
        cdef double dy2i = 0.5*dyi

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):

                # compute change in pressure
                delp = delp + fabs( p1[j, i] - p2[j, i] )

                # sum kinetic energy
                _ke = _ke + 0.5 * ( uc[j, i]*uc[j, i] + vc[j, i]*vc[j, i] )

                # vorticity
                vort[j, i] = dy2i * (uc[j+1, i] - uc[j-1, i]) - dx2i * (vc[j, i+1] - vc[j, i-1])

        self.delp = delp
        self.delke = _ke - self.ke
    
        self.ke = _ke

    cpdef interpolate_velocity(self, int stage=0):
        cdef NpyGrid grid = self.grid

        cdef np.ndarray[ndim=2, dtype=np.float64_t] u = grid.grid['u%d'%stage][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] v = grid.grid['v%d'%stage][0]

        cdef np.ndarray[ndim=2, dtype=np.float64_t] uc = grid.grid['uc'][0]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] vc = grid.grid['vc'][0]

        cdef np.ndarray[ndim=2, dtype=np.int32_t] mapping = grid.grid['mapping'][0]

        # locals
        cdef int i, j

        for j in range(1, self.jmax+1):
            for i in range(1, self.imax+1):
                uc[j, i] = (0.5 * ( u[j, i] + u[j, i-1] ))*(1.0 - mapping[j,i])
                vc[j, i] = (0.5 * ( v[j, i] + v[j-1, i] ))*(1.0 - mapping[j,i])
