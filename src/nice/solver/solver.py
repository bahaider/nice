"""General class for the Finite Difference Method

The solver's role is to marshall the simulation by repeatedly calling
all ancillary functions and the main integration routine to advance
the solution to the next time step.

The ancillary functions handle and I/O and call any pre and post
integration routines.

The integration of the state variables is handled jointly by the
Integrator and the function handling the incompressible algorithm. For
example, each of the different algorithms like the pressure projection
method, projection and PISO algorithms will have different functions
associated with them.

"""

from optparse import OptionParser, OptionGroup, Option
from os.path import basename, splitext, abspath, join
from time import time
import sys

import numpy

# integrators
from integrator import SGPCVanKannIntegrator2D, CGPressureCorrection2D
from schemes import SGPressureCorrection2D
from integrator import EDAC2D

from utils import mkdir, update_progress

class Solver(object):
    """Main solver object object that marshalls the simulation"""
    def __init__(
            self, dim, grid, Re=100., dt=1e-3, tf=1.0,
            pfreq=100,iteration_count=0, current_time=0., 
            cfl=0.5, constant_dt=False, fname=None, 
            steady_state=False, steady_state_tol=1e-3, max_iter=100000,
            geomfile=None, is_moving=False):
        """Constructor

        Parameters:

        dim : int
            Problem dimensionality

        grid : nice.mesh.grid.NpyGrid
            Grid for the IBM calculation

        Re : double
            Reynolds number

        dt : double 
            Recommended time step.

        tf : double
            Final time
            
        pfreq : int
            Output print frequency

        iteration_count : int (0)
            Initial value for the  counter. This is needed for restarts

        current_time : double (0.)
            Initial value for the time counter. This is needed for restarts

        cfl : double (0.5)
            CFL number for time-step control

        constant_dt : bool (False)
            Flag to use constant time steps

        steady_state : bint
            Flag to indicate a steady state problem.

        max_iter : int
            Maximum number of iterations for the PPE

        steady_state_tol : double
            Tolerence check to determine convergence

        geomfile : str
            Geometry filename for the immersed boundary

        is_moving : bint
            Flag to indicate whether the geometry is moving
        
        """
        # problem dimensionality and grid
        self.dim = dim
        self.grid = grid
        self.Re = Re

        # timing attributes and I/O defaults
        self.dt = dt
        self.tf=tf
        self.cfl = cfl
        self.constant_dt = constant_dt

        self.iteration_count=iteration_count
        self.current_time=current_time
        self.pfreq = pfreq

        self.max_iter = max_iter
        
        # output file name
        if fname is None:
            fname = sys.argv[0].split('.')[0]
        self.fname=fname

        # the remaining are the arguments
        self.args = sys.argv[1:]

        # setup the option parser
        self._setup_optparse()

        # list of pre and post intergation functions
        self.pre_integration_functions = []
        self.post_integration_functions = []

        # initialize number of iterations for the PPE to 0
        self.niter = 0

        # flag for a steady state problem
        self.steady_state = steady_state
        self.steady_state_tol = steady_state_tol
        self.has_conveged = False

        # flag for dirichlet pressure at the outlet
        self.outlet_pressure_dirichlet=False

        # geometry information
        self.with_ibm = False
        self.geom = None
        self.geomfile = geomfile
        self.is_moving = is_moving
        if geomfile is not None:
            geom = Geometry2D(geomfile)
            self.setup_geometry(geom, is_moving)

        # velocity boundary conditions
        self.vbc_north = []
        self.vbc_south = []
        self.vbc_east  = []
        self.vbc_west  = []

        # setup the scheme
        self.setup_scheme()

    def set_immersed_boundary_manager(self, ibm_manager, is_moving):
        self.ibm_manager = ibm_manager

        self.geom = geom = ibm_manager.geom
        self.geomfile = geom.fname
        self.is_moving = is_moving

        self.with_ibm = True

        # initially map the cells
        ibm_manager.map_cells()

    def set_steady_state_problem(self):
        self.steady_state = True

    def set_steady_state_tolerance(self, tol):
        self.steady_state_tol = tol

    def set_bc_west(self, bc):
        "Set the boundary condition on the west (left) face of the domain"
        bc.set_face("W")
        self.vbc_west.append(bc)

    def set_bc_east(self, bc):
        "Set the boundary condition on the east (right) face of the domain"
        bc.set_face("E")
        self.vbc_east.append(bc)

    def set_bc_north(self, bc):
        "Set the boundary condition for the north face of the domain"
        bc.set_face("N")
        self.vbc_north.append(bc)
        
    def set_bc_south(self, bc):
        "Set the boundary condition for the south face of the domain"
        bc.set_face("S")
        self.vbc_south.append(bc)

    def set_velocity_bc(self, stage=0):
        for bc in self.vbc_north:
            bc.apply_bc(stage)

        for bc in self.vbc_south:
            bc.apply_bc(stage)

        for bc in self.vbc_east:
            bc.apply_bc(stage)
            
        for bc in self.vbc_west:
            bc.apply_bc(stage)

    def set_pressure_bc_north(self, bc):
        self.pressure_bc_north = bc
        self.pressure_bc_north.set_face("N")

    def set_pressure_bc_south(self, bc):
        self.pressure_bc_south = bc
        self.pressure_bc_south.set_face("S")

    def set_pressure_bc_west(self, bc):
        self.pressure_bc_west = bc
        self.pressure_bc_west.set_face("W")

    def set_pressure_bc_east(self, bc):
        self.pressure_bc_east = bc
        self.pressure_bc_east.set_face("E")

    def set_pressure_bc(self, stage):
        self.pressure_bc_north.apply_bc(stage)
        self.pressure_bc_south.apply_bc(stage)

        self.pressure_bc_east.apply_bc(stage)
        self.pressure_bc_west.apply_bc(stage)

    def setup_scheme(self):
        raise NotImplementedError("Solver::setup_scheme called")

    def solve(self):
        """Marshalling routine for the simulation

        In every iteration step, the solver does the following:
            - call output if time is right
            - call get_time_step to get the new stable time step
            - call pre integration routines
            - call integrate to advance the solution
            - call post integration routines

        """
        dt = self.dt
        tf = self.tf
        current_time = self.current_time
        iteration_count = self.iteration_count

        time_counter = 0.0

        final_time = tf
        if self.steady_state:
            self.has_conveged = False
            final_time = 100000.0

        t1 = time()
        while current_time < final_time:
            # I/O
            if ( (iteration_count % self.pfreq) == 0 ):
                self.output(iteration_count, current_time, dt)

            if ( current_time + dt > final_time ):
                print '\nAdjusting dt to land on final time...'
                dt = final_time - current_time

            # call any functions prior to the integration stage
            for func in self.pre_integration_functions:
                func.evaluate( iteration_count, current_time, dt )
                
            # integrate with the correct time step
            self.integrate( iteration_count, current_time, dt )

            # write output to screen
            if self.verbose:
                self.write_stats(iteration_count, dt)

            # update counters
            current_time += dt
            iteration_count += 1

            # call any post-integrator functions
            for func in self.post_integration_functions:
                func.evaluate( iteration_count, current_time, dt )

            # calculate stable time step for the next iteration
            dt = self.get_time_step()

            # check for convergence
            if self.steady_state:
                self.check_convergence(iteration_count)

            if self.has_conveged:
                break

        solution_time = time() - t1
        print "Completed %d iterations in %g seconds"%(iteration_count, solution_time)

        # final output
        self.output(iteration_count, current_time, dt)
        self.current_time = current_time
                
    def set_pre_integration_function(self, func):
        self.pre_integration_functions.append(func)

    def set_post_integration_function(self, func):
        self.post_integration_functions.append(func)

    def set_ppe_max_iter(self, max_iter):
        self.max_iter = max_iter

    def output(self, iteration_count, current_time, dt):
        fnamebase = self.fname + '_%03d'%iteration_count
        fname = join( self.path, fnamebase )

        # use NumPy's savez to save the output
        print 'Writing output %s at %g s'%(fname, current_time)
        self.scheme.compute_stats()

        grid = self.grid; arrays = {}
        array_names=grid.get_field_names()
        for name in array_names:
            arrays[name] = grid.get_field(name)

        numpy.savez(fname, dt=dt,
                    iteration_count=iteration_count, current_time=current_time,
                    nrows=grid.nrows, ncols=grid.ncols, dx=grid.dx, dy=grid.dy,
                    ke=grid.ke, **arrays)

    def setup(self):
        """Process all command line options"""
        self._process_command_line()
        options = self.options

        # set the options
        self.fname = options.fname
        self.pfreq = options.pfreq

        # set the acceleration function cfl
        if options.cfl is not None:
            self.cfl = options.cfl

        # use constant time steps
        self.constant_dt = options.constant_dt
        
        # final time
        if options.tf:
            self.tf = options.tf

        # verbosity
        self.verbose = options.verbose

    def integrate(self, current_time, iteration_count, dt):
        raise RuntimeError('Solver::integrate called')

    def _process_command_line(self):
        (options, args) = self.parser.parse_args(self.args)
        self.options = options
        
        #save the path where we want to dump output
        self.path = abspath(options.outdir)
        mkdir(self.path)        
                    
    def _setup_optparse(self):
        usage = """
        python %prog [options] """
        self.parser = parser = OptionParser(usage)

        # Add some default options
        parser.add_option("--directory", action="store", type="string",
                         dest="outdir", default=self.fname+'_output',
                         help="""The output directory""")

        parser.add_option("--fname", action="store", type="string",
                         dest="fname", default=self.fname,
                         help="Output file name")

        parser.add_option("--pfreq", action="store", type="int",
                         dest="pfreq", default=self.pfreq,
                         help="Output print frequency")

        parser.add_option("--verbose", action="store_true", dest="verbose",
                          default=False)

        time = OptionGroup(parser, "Time Options", "Time step options")

        time.add_option("--tf", action="store", type="float",
                            dest="tf", default=None,
                            help="Final time")

        time.add_option("--cfl", action='store', type='float',
                        dest='cfl', default=None, help='CFL number')

        time.add_option("--constant-dt", action="store_true", 
                        dest="constant_dt", default=self.constant_dt,
                        help="Use a constant time step")

        parser.add_option_group(time)

class SGPressureCorrectionSolver2D(Solver):
    def set_donor_cell_discretization_gamma(self, gamma=0.9):
        self.scheme.gamma = gamma

    def set_outlet_pressure_dirichlet(self, val):
        self.outlet_pressure_dirichlet=val

    def write_stats(self, iteration_count, dt):
        niter = self.scheme.niter; delke = self.scheme.delke; max_div=self.scheme.max_div
        print "Iteration %d, dt=%g, niter=%g, delke=%g, div=%g"%(
            iteration_count, dt, niter, delke, max_div)

    def setup_scheme(self):
        self.scheme = SGPressureCorrection2D(self.grid, self.Re)

    def check_convergence(self, iteration_count):
        if iteration_count > 1:
            if self.scheme.delke < self.steady_state_tol:
                self.has_conveged = True

    def integrate(self, iteration_count, current_time, dt):

        a1 = 1.5; a2 = 0.5
        if iteration_count == 0:
            a1 = 1.0; a2 = 0.0
        
        scheme = self.scheme
        penalty_ibm=False
        if self.with_ibm:
            penalty_ibm=True

        # set boundary conditions for the velocity
        self.set_velocity_bc(stage=0)

        # compute the RHS of the Momentum equations. 
        scheme.compute_vstar(dt, a1, a2, stage=0, penalty_ibm=penalty_ibm)

        # Boundary conditions for vstar
        scheme.vstar_boundary_conditions(stage=0)

        # compute RHS of the PPE
        scheme.compute_rhs_ppe(dt)

        # Solve the PPE and return the number of iterations
        scheme.solve_ppe_gssor(self.max_iter, self.outlet_pressure_dirichlet)

        # correct the velocity
        scheme.correct_velocity(dt, stage=0)

        # check for steady state convergence
        scheme.interpolate_velocity(stage=0)
        self.set_velocity_bc(stage=0)
        scheme.compute_stats(stage=0)

    def get_time_step(self):
        if self.constant_dt:
            return self.dt
        else:
            grid = self.grid
            u = grid.grid['u0'][0]
            v = grid.grid['v0'][0]

            dx2i = 1./grid.dx*grid.dx
            dy2i = 1./grid.dy*grid.dy

            # viscosity limited time-step
            dt1 = 0.5 * self.Re * 1./(dx2i + dy2i)

            # CFL limited time-step
            dt2 = grid.dx/max( abs(u.max()), abs(u.min()) )
            dt3 = grid.dy/max( abs(v.max()), abs(v.min()) )

            return self.cfl * min( dt1, dt2, dt3 )

    @classmethod
    def dtype(self):

        return numpy.dtype([ 

            # cell centers (x, y, z)
            ('centroid', '3float64'),
            ('centroidu', '3float64'),
            ('centroidv', '3float64'),

            # mapping
            ('mapu', 'int32'), ('mapv', 'int32'), ('mapping', 'int32'),
            
            # pressure and swap buffer for PPE
            ('p', 'float64'), ('pnew', 'float64'), ('pold', 'float64'),
            
            # velocities at face centers (u, v, w)
            ('u0', 'float64'), ('v0', 'float64'),
            
            # velocities at cell centers
            ('uc', 'float64'), ('vc', 'float64'),

            # non-linear (convective) terms
            ('Nx', 'float64'), ('Ny', 'float64'),
            ('Nx1', 'float64'), ('Ny1', 'float64'),

            # viscous (diffusive) terms
            ('Dx', 'float64'), ('Dy', 'float64'),
            ('Dx1', 'float64'), ('Dy1', 'float64'),

            # intermediate velocities (F (ustar), G(vstar))
            ('F', 'float64'), ('G', 'float64'), 
            
            # rhs for the PPE
            ('rhsp', 'float64'),

            # vorticity and divergence
            ('vort', 'float64'), ('div', 'float64')
        
        ])

class CGPressureCorrectionSolver2D(Solver):
    def setup_scheme(self):
        self.scheme = CGPressureCorrection2D(self.grid, self.Re)

    def check_convergence(self, iteration_count):
        if iteration_count > 1:
            if self.scheme.delke < self.steady_state_tol:
                self.has_conveged = True

    def integrate(self, iteration_count, current_time, dt):

        a1 = 1.5; a2 = 0.5
        if iteration_count == 0:
            a1 = 1.0; a2 = 0.0
        
        scheme = self.scheme

        # set boundary conditions for the velocity
        self.set_velocity_bc()

        # compute the RHS of the Momentum equations. 
        scheme.compute_vstar(dt, a1, a2)

        # interpolate face centered velocities
        scheme.interpolate_face_velocities(dt)

        # Boundary conditions for vstar
        scheme.vstar_boundary_conditions()

        # compute RHS of the PPE
        scheme.compute_rhs_ppe(dt)

        # Solve the PPE and return the number of iterations
        scheme.solve_ppe_gssor()

        # correct the velocity
        scheme.correct_velocity(dt)

        # compute kinetic energy and other quantities (vorticity etc)
        scheme.compute_stats()

    def get_time_step(self):
        if self.constant_dt:
            return self.dt
        else:
            grid = self.grid
            u = grid.grid['u0'][0]
            v = grid.grid['v0'][0]

            dx2i = 1./grid.dx*grid.dx
            dy2i = 1./grid.dy*grid.dy

            # viscosity limited time-step
            dt1 = 0.5 * self.Re * 1./(dx2i + dy2i)

            # CFL limited time-step
            dt2 = grid.dx/max( abs(u.max()), abs(u.min()) )
            dt3 = grid.dy/max( abs(v.max()), abs(v.min()) )

            return self.cfl * min( dt1, dt2, dt3 )

    @classmethod
    def dtype(self):

        return numpy.dtype([ 

            # cell centers (x, y, z)
            ('centroid', '3float64'), 

            # mapping
            ('mapping', 'int32'),
            
            # pressure and swap buffer for PPE
            ('p', 'float64'), ('pnew', 'float64'), ('pold', 'float64'),
            
            # velocities at face centers (u, v, w)
            ('U', 'float64'), ('V', 'float64'),
            ('Ustar', 'float64'), ('Vstar', 'float64'),
            ('Ubar', 'float64'), ('Vbar', 'float64'),
            
            # velocities at cell centers
            ('u0', 'float64'), ('v0', 'float64'),
            ('ustar', 'float64'), ('vstar', 'float64'),
            ('ubar', 'float64'), ('vbar', 'float64'),

            # non-linear terms
            ('Nx', 'float64'), ('Ny', 'float64'),
            ('Nx1', 'float64'), ('Ny1', 'float64'),

            # viscous terms
            ('Dx', 'float64'), ('Dy', 'float64'),
            ('Dx1', 'float64'), ('Dy1', 'float64'),

            # rhs for the PPE
            ('rhsp', 'float64'),

            # vorticity
            ('vort', 'float64')
        
        ])

class EDACSolver2D(Solver):
    def write_stats(self, iteration_count, dt):
        delke = self.scheme.delke; max_div=self.scheme.max_div
        print "Iteration %d, dt=%g, delke=%g, div=%g"%(
            iteration_count, dt, delke, max_div)

    def setup_scheme(self):
        self.scheme = EDAC2D(self.grid, self.Re)
        self.nstages = 3

    def set_mach_number(self, Ma):
        self.Ma = Ma
        self.scheme.set_mach_number(Ma)

    def check_convergence(self, iteration_count):
        if iteration_count > 1:
            if self.scheme.delke < self.steady_state_tol:
                self.has_conveged = True

    def integrate(self, iteration_count, current_time, dt):
        scheme = self.scheme

        penalty_ibm=False
        if self.with_ibm:
            penalty_ibm=True

        for stage in range(self.nstages):
            
            # set boundary conditions for the velocity
            self.set_velocity_bc(stage)

            # set boundary conditions for the pressure
            self.set_pressure_bc(stage)

            # compute the RHS of the Momentum equations. 
            scheme.compute_rhs(dt, stage, penalty_ibm)

            # update variables
            scheme.update(dt, stage, self.nstages)

        # check for steady state convergence
        self.set_velocity_bc(stage=0)
        scheme.compute_stats()

    def get_time_step(self):
        if self.constant_dt:
            return self.dt
        else:
            grid = self.grid
            
            dxi = 1./grid.dx
            dyi = 1./grid.dy
            
            dxi2 = dxi*dxi
            dyi2 = dyi*dyi

            u = grid.grid['u0'][0]
            v = grid.grid['v0'][0]

            # viscosity limited time-step
            dt1 = 0.5 * self.Re * 1./(dxi2 + dyi2)

            # CFL limited time-step
            dt2 = 1./( dxi + dyi + numpy.sqrt(1./self.Ma*(dxi2 + dyi2)) )

            return self.cfl * min( dt1, dt2 )

    @classmethod
    def dtype(self):

        return numpy.dtype([ 

            # cell centers (x, y, z)
            ('centroid', '3float64'), 

            # mapping
            ('mapping', 'int32'),
            
            # pressure and swap buffer for PPE
            ('p0', 'float64'),
            ('p1', 'float64'),
            ('p2', 'float64'),
            
            # velocities at face centers (u, v, w)
            ('u0', 'float64'), ('v0', 'float64'),
            ('u1', 'float64'), ('v1', 'float64'),
            ('u2', 'float64'), ('v2', 'float64'),
            
            # velocities at cell centers
            ('uc', 'float64'), ('vc', 'float64'),

            # divergence, laplacians and gradients
            ('lapu', 'float64'),
            ('lapv', 'float64'),
            ('lapp', 'float64'),
            ('div', 'float64'),
            ('divvx', 'float64'),
            ('divvy', 'float64'),
            ('px',   'float64'),
            ('py',   'float64'),           

            # rhs terms
            ('rhsu', 'float64'), ('rhsv', 'float64'), ('rhsp', 'float64'),

            # vorticity
            ('vort', 'float64')
        
        ])

class SGPCVanKannSolver2D(Solver):
    """Solver for the staggered grid, pressure correction solver using
    the Van Kann discretization scheme"""
    def __init__(
        self, dim, grid, boundary_condition, Re=100., dt=1e-3, tf=1.0,
        pfreq=100, iteration_count=0, current_time=0., cfl=0.5, constant_dt=False,
        uinflow=1., moving_boundary=False):

        # base class initialization
        super(SGPCVanKannSolver2D, self).__init__(
            dim=dim, grid=grid, boundary_condition=boundary_condition, Re=Re, 
            dt=dt, tf=tf, pfreq=pfreq, iteration_count=iteration_count, current_time=current_time,
            cfl=cfl, constant_dt=constant_dt)

        # set-up the integrator
        self.integrator = SGPCVanKannIntegrator2D(
            grid, boundary_condition, uinflow, moving_boundary)

    def integrate(self, iteration_count, current_time, dt):
        # get the integrator
        integrator = self.integrator
        
        #### Begin Solver Loop ####

        # get the predicted velocities from the NS equations
        integrator.get_vstar(self.Re, dt, iteration_count)

        # add the ibm force due to the solid object
        integrator.add_ibm_force(dt)

        # set the boundary values for the predicted velocities
        integrator.set_boundary_vstar()

        # set-up the matrices for the ppe and solve
        integrator.set_rhs_ppe(dt)
        integrator.solve_ppe()

        # correct the pressure and velocity
        integrator.update_pv(dt)

        #### End Solver Loop ####

    def get_time_step(self):
        if self.constant_dt:
            return self.dt
