"""Implementation for the Geometry"""

import numpy as np

class Geometry2D:
    def __init__(self, fname):
        self.fname = fname
        self.read_geom_file(fname)

    def read_geom_file(self, fname):
        data = np.loadtxt(fname)
        
        # the last point is the same as the first
        self.nelements = ne = len(data) - 1
        
        # get the vertices for the geometry
        self.vertices = vertices = np.zeros( shape=(ne+1, 2) )
        vertices[:,0] = data[:, 0]
        vertices[:,1] = data[:, 1]

        # allocate space for the control points, normals and tangents
        self.control_points = pnts = np.zeros( shape=(ne, 2) )
        self.normals = normals = np.zeros( shape=(ne, 2) )
        self.tangents = tangents = np.zeros( shape=(ne, 2) )
        
        # get the control points, normals and tangents for the geometry
        for i in range(ne):
            node1 = vertices[i+1]
            node2 = vertices[i]

            pnts[i] = 0.5 * (node1 + node2)
            
            tang = node2 - node1
            tang /= np.sqrt( tang.dot(tang) )
            
            norm = np.array( [-tang[1], tang[0]] )

            tangents[i] = tang[:]
            normals[i, 0] = -tang[1]
            normals[i, 1] =  tang[0]

        self.xmin = np.min( vertices[:, 0] )
        self.xmax = np.max( vertices[:, 0] )

        self.ymin = np.min( vertices[:, 1] )
        self.ymax = np.max( vertices[:, 1] )
            
    @classmethod
    def write_cylinder(self, fname="cylinder", rad=1, npanels=120, xc=0.0, yc=0.0):
        """Create a geometry file"""
        theta = np.linspace( 0, 2*np.pi, npanels+1 )
        x = rad*np.cos(theta); x+= xc
        y = rad*np.sin(theta); y+= yc

        data = np.empty( shape=(x.size, 2) )
        data[:, 0] = x
        data[:, 1] = y

        np.savetxt( fname+'.dat', data )

    @classmethod
    def write_box(self, fname='box', a=1.0, b=1.0, xc=0.0, yc=0.0):
        x = np.array( [a/2, a/2, 0, -a/2, -a/2, -a/2, 0, a/2, a/2] ) + xc
        y = np.array( [0, b/2, b/2, b/2, 0, -b/2, -b/2, -b/2, 0] ) + yc

        data = np.empty( shape=(x.size, 2) )
        data[:, 0] = x
        data[:, 1] = y

        np.savetxt( fname+'.dat', data )
