"""Implementation for the IBM"""

import numpy as np
cimport numpy as np

from nice.mesh.grid cimport NpyGrid

cdef class ImmersedBoundaryManager:
    def __init__(self, NpyGrid grid, object geom):
        self.grid = grid
        self.geom = geom

    cpdef map_cells(self):
        cdef NpyGrid grid = self.grid
        cdef object geom = self.geom
        cdef bint is_staggered = grid.is_staggered

        cdef np.ndarray[ndim=3, dtype=np.float64_t] centroids
        cdef np.ndarray[ndim=2, dtype=np.float64_t] xc, yc
        cdef np.ndarray[ndim=2, dtype=np.int32_t] mapping

        # pressure map
        centroids = grid.grid['centroid'][0]
        mapping = grid.grid['mapping'][0]
        xc = centroids[:, :, 0]
        yc = centroids[:, :, 1]
        self._map_cells(centroids, mapping, xc, yc)

        if is_staggered:
            # u-velocity map
            centroids = grid.grid['centroidu'][0]
            mapping = grid.grid['mapu'][0]
            xc = centroids[:, :, 0]
            yc = centroids[:, :, 1]
            self._map_cells(centroids, mapping, xc, yc)

            # v-velocity map
            centroids = grid.grid['centroidv'][0]
            mapping = grid.grid['mapv'][0]
            xc = centroids[:, :, 0]
            yc = centroids[:, :, 1]
            self._map_cells(centroids, mapping, xc, yc)            

    cdef _map_cells(
        self, np.ndarray[ndim=3, dtype=np.float64_t] centroids,
        np.ndarray[ndim=2, dtype=np.int32_t] mapping,
        np.ndarray[ndim=2, dtype=np.float64_t] xc,
        np.ndarray[ndim=2, dtype=np.float64_t] yc):

        cdef NpyGrid grid = self.grid
        cdef object geom = self.geom

        # geom control points and normals
        cdef int nelements = geom.nelements
        cdef np.ndarray[ndim=2, dtype=np.float64_t] control_points = geom.control_points
        cdef np.ndarray[ndim=1, dtype=np.float64_t] cx = control_points[:, 0]
        cdef np.ndarray[ndim=1, dtype=np.float64_t] cy = control_points[:, 1]
        cdef np.ndarray[ndim=2, dtype=np.float64_t] normals = geom.normals
        
        # min and max coordinates for the geom
        cdef double xmin = geom.xmin - grid.dx
        cdef double xmax = geom.xmax + grid.dx

        cdef double ymin = geom.ymin - grid.dy 
        cdef double ymax = geom.ymax + grid.dy
        
        # locals
        cdef int imax = grid.imax
        cdef int jmax = grid.jmax
        cdef int i, j, k

        cdef int kmin
        cdef double dist2, min_dist

        cdef np.ndarray[ndim=1, dtype=np.float64_t] vec1 = np.array( [0.0, 0.0] )

        # iterate over each cell and map whether inside or outside
        for j in range(1, jmax+1):
            for i in range(1, imax+1):

                # do a prelimnary check for this cell
                if ( (xc[j,i] > xmin) and (xc[j,i] < xmax) and\
                     (yc[j,i] > ymin) and (yc[j,i] < ymax) ):
                    
                    # find the nearest control point to this cell
                    min_dist = 1e20
                    for k in range(nelements):
                        dist2 = (cx[k]-xc[j,i])*(cx[k]-xc[j,i]) + \
                                (cy[k]-yc[j,i])*(cy[k]-yc[j,i])
                        
                        if dist2 < min_dist: 
                            kmin = k
                            min_dist = dist2

                    # now check for containment with the normal
                    vec1[0] = cx[kmin] - xc[j, i]
                    vec1[1] = cy[kmin] - yc[j, i]

                    # all cells within the object will have a positive dot product
                    if vec1.dot( normals[kmin] ) > 0:
                        mapping[j, i] = 1
                        
                # all others are considered as fluid cells
                else:
                    mapping[j, i] = 0
