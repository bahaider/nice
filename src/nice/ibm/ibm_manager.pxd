"""General class for the IBM force"""

import numpy as np
cimport numpy as np

from nice.mesh.grid cimport NpyGrid

cdef class ImmersedBoundaryManager:
     cdef public NpyGrid grid
     cdef public object geom

     cpdef map_cells(self)

     cdef _map_cells(
         self, np.ndarray[ndim=3, dtype=np.float64_t] centroids,
         np.ndarray[ndim=2, dtype=np.int32_t] mapping,
         np.ndarray[ndim=2, dtype=np.float64_t] xc,
         np.ndarray[ndim=2, dtype=np.float64_t] yc)
