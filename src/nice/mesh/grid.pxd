"""Definitions for a Cartesian grid."""

# Numpy
import numpy
cimport numpy

# Local imports
from common cimport cDomain

cdef class Domain:
    """Physcial limits for the simulation domain."""
    cdef cDomain _domain

cdef class NpyGrid:
    """A Cartesian grid using Numpy record arrays.

    A 3D cell and the notation associated with it is defined with reference to 
    the following figure:

                      d________________c
                     /|               /|
                   /  |              / |
                 / dy |       x    /   |
               /      |          /     |
             h------------*-----g      |
              |  x   a|_____dx_|_______|b
              |     /          |      /  
              |   /            |    / dz
              | /        x     |  /
              |                |/
             e------------------f                * -- Cell center
                                                 x -- Face center

               Y 
              |
              | X-Y plane
              |  
    Y-Z plane |__________ X
             /  
            /  Z-X plane
           /
          Z 

    """
    
    cdef public bint is_staggered             # Flag for staggered grids
    cdef public Domain domain                 # domain extents for the grid
    cdef public numpy.ndarray grid            # numpy record array grid cells
    cdef public numpy.dtype dtype             # record array data type for the grid cells

    cdef public double dx, dy, dz             # mesh spacing along x, y
    cdef public int ncells, ncx, ncy, ncz     # number of cells along each coordinate direction
    cdef public int nrows, ncols, npages      # number of cells along indexing direction
    cdef public int imax, jmax, kmax          # index ranges
    cdef public int ncells_physical           # total number of physical cells
    cdef public double volume                 # volume of the physical domain

    cdef public double ke                     # global energy in the system
