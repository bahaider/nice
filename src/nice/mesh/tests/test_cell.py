"""Unittests for the Cell"""

# NICE imports
from nice.mesh import cell

# Python unittests and NumPy
import numpy
import unittest

class CellTestCase(unittest.TestCase):
    """Basic tests for the Cell defined in cell.pyx
    
    We create a cell and test each method thereafter. The default cell
    we create has the following properties.

    dim = 2; ix = 1, iy = 2; iz = 0; cx = 1.0, cz = 1.0, cz = 0
    dx = dy = 0.5

    """
    def setUp(self):
        # create the default cell
        self.cell = cell.Cell(
            dim=2, ix=1, iy=2, iz=0, cx=1.0, cy=1.0, cz=0.0,
            dx=0.5, dy=0.25, dz=0.5)

    def test_get_centroid(self):
        "Cell :: test the get centroid function"
        cx, cy, cz = self.cell.get_centroid()

        self.assertAlmostEqual( cx, 1.0, 14 )
        self.assertAlmostEqual( cy, 1.0, 14 )
        self.assertAlmostEqual( cz, 0.0, 14 )

    def test_get_spacings(self):
        "Cell :: test the get spacing functions"
        dx, dy, dz = self.cell.get_spacings()
        
        self.assertAlmostEqual( dx, 0.5, 14 )
        self.assertAlmostEqual( dy, 0.25, 14 )

        # dz should be 1 for a 2D cell
        self.assertAlmostEqual( dz, 1.0, 14 )                                

    def test_get_vertices(self):
        "Cell :: test the get vertices function"
        vertices = self.cell.get_vertices()
        cx, cy, cz = self.cell.get_centroid()
        dx, dy, dz = self.cell.get_spacings()

        a = vertices[:, 0] # lower left
        self.assertAlmostEqual( a[0], cx - 0.5*dx, 14 )
        self.assertAlmostEqual( a[1], cy - 0.5*dy, 14 )

        b = vertices[:, 1] # lower right
        self.assertAlmostEqual( b[0], cx + 0.5*dx, 14 )
        self.assertAlmostEqual( b[1], cy - 0.5*dy, 14 )

        c = vertices[:, 2] # upper right
        self.assertAlmostEqual( c[0], cx + 0.5*dx, 14 )
        self.assertAlmostEqual( c[1], cy + 0.5*dy, 14 )

        d = vertices[:, 3] # upper left
        self.assertAlmostEqual( d[0], cx - 0.5*dx, 14 )
        self.assertAlmostEqual( d[1], cy + 0.5*dy, 14 )
        
    def test_get_volume(self):
        "Cell :: test the get volume function"
        volume = self.cell.get_volume()
        dx, dy, dz = self.cell.get_spacings()

        self.assertAlmostEqual( volume, dx*dy, 14 )

    def test_get_indices(self):
        "Cell :: test the get indices function"
        ix, iy, iz = self.cell.get_indices()

        self.assertEqual(ix, 1)
        self.assertEqual(iy, 2)
        self.assertEqual(iz, 0)

if __name__ == '__main__':
    unittest.main()
