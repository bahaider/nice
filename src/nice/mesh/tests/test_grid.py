"""Unittests for the NICE Grid"""

# NICE imports
from nice.mesh import grid

# Python unittests and NumPy
import numpy
import unittest

class Grid2DTestCase(unittest.TestCase):
    """Tests for the Grid2D object.

    We create a grid in 2D with the following properties

    (xmin, xmax) = (-0.5, -0.25)
    (xmax, ymax) = (1.0, 0.5)

    Initially, we request the mesh spacings dx = 0.33, dy = 1./3

    Since dx and dy are not integer multiples of the input domain. The
    mesh spacings will be adjusted accordingly. The domain limits will
    be adjusted with the adjusted spacings to include one ghost cell
    along each coordinate direction.

    With these values, we will require 5 cells along x and 3 cells
    along y to cover the domain. The adjusted cell spacings are given
    by dx = 1.5/5  = 0.3 and dy = 0.75/3 = 0.25

    With ghost cells, ncx = 7 and ncy = 5, giving a total of 35 cells

    """
    def setUp(self):
        self.domain = domain = grid.Domain(
            dim=2, xmin=-0.5, ymin=-0.25, zmin=0.0,
            xmax=1.0, ymax=0.5, zmax=0.0)
        dx = dy = 1./3

        self.grid = grid.Grid2D(
            domain, dx, dy)
            
    def test_new_spacings(self):
        "Grid2D :: test adjusted spacings"
        dx, dy = self.grid.get_spacings()
        self.assertAlmostEqual( dx, 0.30, 10 )
        self.assertAlmostEqual( dy, 0.25, 10 )

    def test_domain_xmin(self):
        "Grid2D :: test domain xmin"
        xmin, ymin, zmin = self.domain.get_xmin()
        dx, dy = self.grid.get_spacings()

        self.assertAlmostEqual(xmin, -0.50-dx, 14)
        self.assertAlmostEqual(ymin, -0.25-dy, 14)
        self.assertAlmostEqual(zmin, 0.0, 14)

    def test_domain_xmax(self):
        "Grid2D :: test domain xmax"
        xmax, ymax, zmax = self.domain.get_xmax()
        dx, dy = self.grid.get_spacings()

        self.assertAlmostEqual(xmax, 1.0+dx, 14)
        self.assertAlmostEqual(ymax, 0.5+dy, 14)
        self.assertAlmostEqual(zmax, 0.0, 14)

    def test_ncells(self):
        "Grid2D :: test ncells"
        ncells = self.grid.ncells
        ncx = self.grid.ncx
        ncy = self.grid.ncy

        self.assertEqual(ncx, 7)
        self.assertEqual(ncy, 5)
        self.assertEqual(ncells, 35)

    def test_is_valid_index(self):
        "Grid2D :: test valid cell index"
        self.assertRaises( ValueError, self.grid.get_cell, 35 )

    def test_cells(self):
        "Grid2D :: test cells"
        ncells = self.grid.ncells
        ncx = self.grid.ncx
        ncy = self.grid.ncy

        dx, dy = self.grid.get_spacings()
        xmin, ymin = self.grid.get_xmin()
        xmax, ymax = self.grid.get_xmax()
        
        for i in range(ncells):
            c = self.grid.get_cell(i)
            
            ix, iy, iz = c.get_indices()
            cx, cy, cz = c.get_centroid()

            index = grid.py_flatten2D(ix, iy, ncy)
            self.assertEqual( index, i )
            
            self.assertEqual( cx, xmin + (0.5 + ix)*dx, 14 )
            self.assertEqual( cy, ymin + (0.5 + iy)*dy, 14 )        
        
if __name__ == '__main__':
    unittest.main()
