"""Common definitions for the grid.

A 3D cell and the notation associated with it is defined with reference to 
the following figure:

                      d________________c
                     /|               /|
                   /  |              / |
                 / dy |       x    /   |
               /      |          /     |
             h------------*-----g      |
              |  x   a|_____dx_|_______|b
              |     /          |      /  
              |   /            |    / dz
              | /        x     |  /
              |                |/
             e------------------f                * -- Cell center
                                                 x -- Face center

                
              |y
              |
              |
              |__________ x
             /  
            / 
           /z


The cell-centered variables are defined at the centroid (*), while the face-centered 
nodes (x) are used to define the velocity for mass conservation.

A 2D cell is a 'flattened version' with vertices e, f, g and h identical to a, b, c & d. 
We leave dz in this case to 1 to avoid computing a singular volume dx*dy*dz

"""

# basic 3D point
cdef struct cPoint:
    double x, y, z

# Domian limits
cdef struct cDomain:
    int dim                          # Problem dimensionality
    cPoint xmin, xmax                # domain min and max values
