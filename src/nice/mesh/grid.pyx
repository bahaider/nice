"""Definitions for the Grid objects in NICE"""

# Cyhton helpers for iterating over STL vectors
from cython.operator cimport dereference as deref, preincrement as inc

# NumPy
import numpy
cimport numpy

# math imports
cdef extern from "math.h":
    double ceil( double )
    double floor(double )

cdef class Domain:
    """Physcial limits for the simulation domain."""
    def __init__(self, int dim, double xmin, double ymin, double zmin,
                 double xmax, double ymax, double zmax):
        self._domain.dim = dim

        self._domain.xmin.x = xmin
        self._domain.xmin.y = ymin
        self._domain.xmin.z = zmin

        self._domain.xmax.x = xmax
        self._domain.xmax.y = ymax
        self._domain.xmax.z = zmax

    def get_xmin(self):
        "Return xmin"
        return [self._domain.xmin.x, self._domain.xmin.y, self._domain.xmin.z]

    def get_xmax(self):
        "Return xmin"
        return [self._domain.xmax.x, self._domain.xmax.y, self._domain.xmax.z]

    def __str__(self):
        domain = self._domain
        dim = domain.dim
        if dim == 2:
            s='(xmin, ymin) = (%g, %g), (xmax, ymax) = (%g, %g)'%(domain.xmin.x,
                                                                  domain.xmin.y,
                                                                  domain.xmax.x,
                                                                  domain.xmax.y)
        return s

######################################################################
# NpyGrid class
######################################################################
cdef class NpyGrid:
    """A Cartesian grid using Numpy record arrays.

    A 3D cell and the notation associated with it is defined with reference to 
    the following figure:

                      d________________c
                     /|               /|
                   /  |              / |
                 / dy |       x    /   |
               /      |          /     |
             h------------*-----g      |
              |  x   a|_____dx_|_______|b
              |     /          |      /  
              |   /            |    / dz
              | /        x     |  /
              |                |/
             e------------------f                * -- Cell center
                                                 x -- Face center

               Y 
              |
              | X-Y plane
              |  
    Y-Z plane |__________ X
             /  
            /  Z-X plane
           /
          Z 

    A Cartesian grid is defined as a NumPy record array with data
    fields associated with the type of cell used in the scheme.

    Consider for example a typical staggered grid cell `cStaggeredGridCell`
    defined as
    
    cdef packed struct cStaggeredGridCell:
        double[3] uold
        double[3] unew
        double[3] dudt
        double[2] pvisc

    The associated NumPy record array in this case will look like
    
    cells = np.recarray( shape=(nx, ny, nz), names=['uold', 'unew', 'dudt', 'pvisc'], 
                         formats=['3float64', '3float64', '3float64', '2float64'] )

   Grid indexing:
   --------------

   We have a total of nz X ny X nx cells indexed as pages, rows and
   columns, along the z, y and x directions respectively. For two
   dimensional grids, npages is always 1 and indexing must be done
   apropriately.

   """
    def __init__(self, Domain domain, double dx, double dy, double dz,
                 dtype=None, is_staggered=False):
        """Constructor for the STLGrid2D.

        Parameters:
        -----------

        domain : Domain
            Domain extents for the grid

        dx, dy, dz : double
            Mesh spacing along x,y

        dtype : numpy.dtype object
            Numpy data type assocated with the record array
            
        is_staggered : bool
            Flag if this is a staggered grid

        Notes:
        
        The default data type will create a cell with only x, y and z
        values

        """
        self.is_staggered = is_staggered
        self.domain = domain
        self.dx = dx
        self.dy = dy
        self.dz = dz

        # set-up the grid. The default data type assumes only x, y and
        # z values associated with each cell
        if dtype is None:
            dtype = numpy.dtype([ ('centroid', '3float64') ])
            
        self.dtype = dtype
        self._initialize()

    def get_xmin(self):
        "Return the domain maximum limits"
        domain = self.domain._domain
        return [domain.xmin.x, domain.xmin.y, domain.xmin.z]
    
    def get_xmax(self):
        "Return the domain minimum limits"
        domain = self.domain._domain
        return [domain.xmax.x, domain.xmax.y, domain.xmax.z]

    def get_spacings(self):
        "Return the mesh spacings along each dimension"
        return self.dx, self.dy, self.dz
    
    def get_ncells(self):
        "Return the number of cells in each coordinate direction"
        return [self.ncx, self.ncy, self.ncz]

    def get_cell(self, int i, int j, int k):
        "Return a cell by index"
        return self.grid[i, j, k]

    ##################################################################
    # Private methods
    ##################################################################
    def _initialize(self):
        """Initialize the grid.

        Given a Domain, we check to see if the requested mesh spacing
        is an integer multiple of the physical domain. The spacings
        are appropriately adjusted if this is not the
        case. Additionally, the domain extents are extended to
        accomodate ghost cells.

        Once we know the number of cells and the domain limits, we
        proceed to populate the cells vector with the individual
        cells.

        """
        cdef cDomain domain = self.domain._domain
        
        cdef int dim = domain.dim
        cdef double xmin = domain.xmin.x
        cdef double ymin = domain.xmin.y
        cdef double zmin = domain.xmin.z

        cdef double xmax = domain.xmax.x
        cdef double ymax = domain.xmax.y
        cdef double zmax = domain.xmax.z

        cdef double dx = self.dx
        cdef double dy = self.dy
        cdef double dz = self.dz

        cdef numpy.dtype dtype = self.dtype
        cdef numpy.ndarray grid        
        
        # locals
        print "NpyGrid :: initlialize"
        print "Original dx, dy, dz = %g, %g, %g"%(dx, dy, dz)

        # calculate the number of cells in each direction based on inputs
        cdef int ncx = int( ceil((xmax - xmin)/dx) )
        cdef int ncy = int( ceil((ymax - ymin)/dy) )
        cdef int ncz = 1

        cdef int nrows, ncols, npages

        # one dummy cell in the z direction for 2D geometries
        if dim > 2:
            ncz = int( ceil((zmax - zmin)/dz) )

        # adjusted dx in each direction
        dx = (xmax - xmin)/ncx
        dy = (ymax - ymin)/ncy
        
        if dim > 2:
            dz = (zmax - zmin)/ncz

        # extend domain boundaries for ghost cells
        domain.xmin.x = domain.xmin.x - dx
        domain.xmin.y = domain.xmin.y - dy

        domain.xmax.x = domain.xmax.x + dx
        domain.xmax.y = domain.xmax.y + dy

        if dim > 2:
            domain.xmin.z = domain.xmin.z - dz
            domain.xmax.z = domain.xmax.z + dz
        else:
            domain.xmin.z = -0.5 * dz
            domain.xmax.z = +0.5 * dz

        self.domain._domain = domain

        # total number of physical cells
        self.ncells_physical = ncx * ncy * ncz

        # re-adjust number of cells. +2 in each direction
        ncx = ncx + 2
        ncy = ncy + 2
        if dim > 2:
            ncz = ncz + 2

        self.volume = (xmax - xmin) * (ymax - ymin)
        if dim == 3:
            self.volume *= (zmax - zmin)

        # set the adjested cell spacings data attributes
        self.dx = dx
        self.dy = dy
        self.dz = dz

        self.ncx = ncx
        self.ncy = ncy
        self.ncz = ncz
        self.ncells = ncells = ncx * ncy * ncz
        
        # number of cells along each dimension for indexing
        self.npages = npages = ncz
        self.nrows  = nrows = ncy
        self.ncols  = ncols = ncx

        print "Adjusted dx, dy, dz = %g, %g, %g"%(dx, dy, dz)
        print "Number of cells (including ghosts): npages, nrows, ncols = %d, %d, %d"%(npages, nrows, ncols)

        # set the index ranges for iteration
        self.imax = self.ncols - 2
        self.jmax = self.nrows - 2
        self.kmax = self.npages -2 

        # Create the record array denoting the grid
        grid = numpy.zeros( shape=(npages, nrows, ncols), dtype=dtype )
        self.grid = grid

        cdef numpy.ndarray centroid = grid['centroid']

        # create cells based on their cell center coordinates
        for pagei in range(npages):
            for rowi in range(nrows):
                for coli in range(ncols):
                    cx = domain.xmin.x + (coli + 0.5) * dx
                    cy = domain.xmin.y + (rowi + 0.5) * dy
                    cz = domain.xmin.z + (pagei + 0.5) * dz

                    centroid[pagei, rowi, coli][0] = cx
                    centroid[pagei, rowi, coli][1] = cy
                    centroid[pagei, rowi, coli][2] = cz

        # create the cntroids for the velocity nodes
        if self.is_staggered:
            self.get_staggered_grid_centroids()

    def get_staggered_grid_centroids(self):
        cdef numpy.ndarray grid = self.grid
        
        cdef numpy.ndarray centroid  = grid['centroid']
        cdef numpy.ndarray centroidu = grid['centroidu']
        cdef numpy.ndarray centroidv = grid['centroidv']
        
        # shift the centroids in the appropriate directions
        centroidu[:, :, :, 0][:] = centroid[:, :, :, 0] + self.dx/2.
        centroidu[:, :, :, 1][:] = centroid[:, :, :, 1]

        centroidv[:, :, :, 0][:] = centroid[:, :, :, 0]
        centroidv[:, :, :, 1][:] = centroid[:, :, :, 1] + self.dy/2.

    def _is_valid_index(self, int index):
        size = self.get_ncells()
        if index >= size : 
            msg = 'Invalid cell index %d. Should be < %d'%(index, size)
            raise ValueError(msg)


    ##################################################################
    # Helper functions
    ##################################################################
    def get_field_names(self):
        'Return a list of valid field names'
        return self.dtype.fields.keys()
        
    def get_field(self, str field):
        "Return a field represented by the grid"
        if not self.dtype.fields.has_key(field):
            raise ValueError('Field %s unavailable for this grid')

        return self.grid[field]
