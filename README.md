NICE: Navier-Stokes Immersed boundary Code for insEct flight
-------------------------------------------------------------

NICE is meant to be a central repository for the immersed boundary
method used for the WCCM XI conference. 

Features
---------

  - Unsteady Incompressible Navier Stokes on Cartesian grids
  - Moving boundaries using the Immersed Boundary Method

Solvers:
---------

Currently, we intend to support the ghost fluid IBM for incompressible
NS equations described by Mittal et al


Installation
=============

  - NumPy   : Array support
  - SciPy   : Sparse matrices and linear algebra
  - Cython  : Efficient Python-like code

Optional dependencies:
-----------------------

None as of yet.

Running the examples
---------------------

Check the examples directory for simple examples.
